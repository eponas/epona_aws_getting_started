# patternモジュールを適用する

本ページでは、Getting Started（本リポジトリ）をどのような順番で実行していくのか、その適用順を記載します。

## 概要

ここでは、以下の2つの環境を構築します。

- [Delivery環境](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery)
- [Runtime環境（ステージング環境）](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging)

各ディレクトリ内には、patternモジュールを適用したTerraform構成ファイルが含まれています。

本ページでは、各ディレクトリ内で行う操作の基本と、各ディレクトリの適用順を説明します。

## 前提条件

[セットアップ](./1_environment_setup.md)に記載の手順を実行しており、Terraformを実行するためのユーザーの作成などの作業が完了している必要があります。

また、AWSアカウントやSaaSとの契約などの[前提条件](../README.md#requirements)を満たしているかも再確認してください。

## Terraform実行ユーザーに関するポイント

本リポジトリ内に含まれてるTerraform構成ファイルを適用する際には、ディレクトリに応じてIAMユーザーを切り替える必要があります。

[セットアップ](1_environment_setup.md)で、Terraformを実行するためのIAMユーザーを作成しているはずです。

各ディレクトリとユーザーの対応は、以下のようになります。

<!-- markdownlint-disable MD013 -->

- [delivery](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery)ディレクトリ配下 … Delivery環境用のTerraform実行IAMユーザーを使用
- [runtimes/staging](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging)ディレクトリ配下 … Runtime環境用のTerraform実行IAMユーザーを使用

<!-- markdownlint-enable MD013 -->

セットアップが完了していても、ディレクトリとIAMユーザーの対応を誤った場合はTerraformの実行に失敗します。  
これは、Terraform実行用のIAMロールにスイッチできなくなるからです。ご注意ください。

## 基本的な操作

各ディレクトリ配下では、以下のコマンドを実行するのが基本になります。  
それ以外の操作や注意事項がある場合は、各ディレクトリごとに補足します。

### init

初回実行時に、1度行います。各ディレクトリ単位で実行が必要です。

```shell
$ terraform init
```

:information_source: 利用するProviderやモジュールのバージョンアップ時、追加時などは再度`init`が必要です。

### plan

`plan`を実行することにより、これからどのようなリソースが構築されるか、その実行計画を見ることができます。  
また、リソースを構築した後にTerraformの構成ファイルを変更してから使用すると、これから変更する差分を見ることができます。

```shell
$ terraform plan
```

実際にリソースを構築する前に、必ず`plan`で実行計画を確認しましょう。

`plan`実行時に変数の入力を求められた場合は、実行のために追加の情報が必要です。  
このケースにあたるのは、以下のような情報が該当します。対象のpatternのドキュメントを確認し、適切に値を設定してください。

- 実際の実行時にしかわからない情報（動的に決まる値など）
- 秘匿すべき情報

### apply

`plan`で確認した内容に沿って、実際のリソースを構築します。  
また、リソースを構築した後にTerraformの構成ファイルを変更してから使用すると、変更差分を反映できます。

```shell
$ terraform apply
```

### destroy

:warning: 構築済みのリソースを破棄するコマンドです。

`apply`で構築したリソースを破棄できます。

```shell
$ terraform destroy
```

リソースが不要になった時や、作り直したい時に使用してください。

## 構築順

環境は、本セクションに記載の順で構築していきます。

`clone`された本リポジトリ内にある、記載のディレクトリ内に移動して[基本的な操作](#基本的な操作)で説明したTerraformのコマンドを実行します。

また、 :green_book: のアイコンがついたディレクトリは実行や確認にあたり、補足を記載しています。  
詳しくは各ディレクトリ内の`README.md`を参照してください。

各項目は、Terraformの構成ファイルが配置されたディレクトリと、その適用元になったpatternのドキュメントの参照を記載しています。

### Delivery環境上にCI/CDパイプラインを構築する

Delivery環境に、CIパイプラインを構築します。

---

- このテーマが支えるアクティビティ
  - [デプロイメントパイプライン](https://eponas.gitlab.io/epona/guide/activities#%E3%83%87%E3%83%97%E3%83%AD%E3%82%A4%E3%83%A1%E3%83%B3%E3%83%88%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3)
  - [VCS Provider](https://eponas.gitlab.io/epona/guide/activities#vcs-provider)
  - [インフラセキュリティ](https://eponas.gitlab.io/epona/guide/activities#%E3%82%A4%E3%83%B3%E3%83%95%E3%83%A9%E3%82%BB%E3%82%AD%E3%83%A5%E3%83%AA%E3%83%86%E3%82%A3)

---

<!-- markdownlint-disable MD013 -->

- [delivery/network](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/network) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/network/))
- [delivery/ci_pipeline](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/ci_pipeline):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/))
- [delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/))
- [delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/))
- [delivery/runtime_instances/staging/cd_pipeline_frontend_trigger](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/cd_pipeline_frontend_trigger) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_frontend_trigger/))

<!-- markdownlint-enable MD013 -->

CDパイプラインはRuntime環境で起動しますが、ここではその仕掛けを準備します。

### Runtime環境にアプリケーションを動作させるための環境を作成する

Runtime環境上に、アプリケーションを動作させるための環境を揃えます。

---

- このテーマが支えるアクティビティ
  - [デプロイメントパイプライン](https://eponas.gitlab.io/epona/guide/activities#%E3%83%87%E3%83%97%E3%83%AD%E3%82%A4%E3%83%A1%E3%83%B3%E3%83%88%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3)
  - [バックアップ](https://eponas.gitlab.io/epona/guide/activities#%E3%83%90%E3%83%83%E3%82%AF%E3%82%A2%E3%83%83%E3%83%97)

---

データベースやキャッシュ、コンテナ環境やオブジェクトストレージなどを構築します。この時点では、まだアプリケーションはデプロイされません。

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/network](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/network):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/network/))
- [runtimes/staging/encryption_key](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/encryption_key) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/encryption_key/))
- [runtimes/staging/database](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/database) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/database/))
  - `関連pattern実行後に設定する。`と書かれた部分をコメントアウトして適用
- [runtimes/staging/redis](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/redis) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/redis/))
  - `関連pattern実行後に設定する。`と書かれた部分をコメントアウトして適用
- [runtimes/staging/ses_smtp_user](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/ses_smtp_user):green_book:
- [runtimes/staging/parameter_store](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/parameter_store):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/parameter_store/))
- [runtimes/staging/public_traffic_container_service_backend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/public_traffic_container_service_backend) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/public_traffic_container_service/))
- [runtimes/staging/public_traffic_container_service_notifier](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/public_traffic_container_service_notifier) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/public_traffic_container_service/))
- [runtimes/staging/webacl_frontend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/webacl_frontend) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/webacl/))
- [runtimes/staging/cacheable_frontend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/cacheable_frontend) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cacheable_frontend/))

<!-- markdownlint-enable MD013 -->

### Runtime環境にアプリケーションのCDパイプラインを構築する

Runtime環境に、CDパイプラインを構築します。

---

- このテーマが支えるアクティビティ
  - [デプロイメントパイプライン](https://eponas.gitlab.io/epona/guide/activities#%E3%83%87%E3%83%97%E3%83%AD%E3%82%A4%E3%83%A1%E3%83%B3%E3%83%88%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3)

---

ここで構築したリソースの情報は、Delivery環境へ連携する必要があります。

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/cd_pipeline_backend_backend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/cd_pipeline_backend_backend) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/))
- [runtimes/staging/cd_pipeline_backend_notifier](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/cd_pipeline_backend_notifier) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend/))
- [runtimes/staging/cd_pipeline_frontend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/cd_pipeline_frontend) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_frontend/))

<!-- markdownlint-enable MD013 -->

### Delivery環境にアプリケーションのCDパイプラインを起動する仕組みを構築する

Delivery環境に、CDパイプラインを起動する仕組みを構築します。

---

- このテーマが支えるアクティビティ
  - [デプロイメントパイプライン](https://eponas.gitlab.io/epona/guide/activities#%E3%83%87%E3%83%97%E3%83%AD%E3%82%A4%E3%83%A1%E3%83%B3%E3%83%88%E3%83%91%E3%82%A4%E3%83%97%E3%83%A9%E3%82%A4%E3%83%B3)

---

ここで行う作業は、構築済みのリソースのアップデートになります。

<!-- markdownlint-disable MD013 -->
<!-- textlint-disable -->

- [delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_backend):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/))
- [delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/))
- [delivery/runtime_instances/staging/cd_pipeline_frontend_trigger](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/cd_pipeline_frontend_trigger):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_frontend_trigger/))

<!-- textlint-enable -->
<!-- markdownlint-enable MD013 -->

この後、CIパイプラインを起動させることで、アプリケーションがデプロイ可能となります。
CIパイプラインを起動する方法については[パイプラインを実行する](../delivery/ci_pipeline/README.md#パイプラインを実行する)を参照してください。

CIパイプラインが実行されると、CDパイプラインによってアプリケーションのデプロイが実行されます。
デプロイの進捗はAWS CodePipelineで確認してください。

デプロイが完了したら、ブラウザでアプリケーションにアクセスしてください。
このとき、URLのホスト名は[cacheable_frontend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/cacheable_frontend)インスタンスの`record_name`で指定した値になります。
（例：`https://chat-example.staging.epona-devops.com/`）

本手順で構築したexample-chatには、[ローカルで動かした場合に用意されているテストユーザ](https://github.com/Fintan-contents/example-chat#動作確認をする)は存在しません。
したがって、アプリケーションの動作確認にはサインアップが必要になります。
このとき、メールアドレスにはAmazon SESで検証済みのEメールアドレスを使用してください。

### Runtime環境のログとメトリクスをSaaSに収集し、インシデント管理の仕組みを構築する

Runtime環境に、Datadogとのインテグレーションとログ転送の仕組みを導入します。  
また、インシデント管理のためPagerDutyとのインテグレーションも行います。

---

- このテーマが支えるアクティビティ
  - [モニタリング](https://eponas.gitlab.io/epona/guide/activities#%E3%83%A2%E3%83%8B%E3%82%BF%E3%83%AA%E3%83%B3%E3%82%B0)
  - [コミュニケーション](https://eponas.gitlab.io/epona/guide/activities#%E3%82%B3%E3%83%9F%E3%83%A5%E3%83%8B%E3%82%B1%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3)

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/datadog_integration](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_integration) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_integration/))
- [runtimes/staging/datadog_forwarder](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_forwarder) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_forwarder/))
- [runtimes/staging/datadog_forwarder_frontend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_forwarder_frontend)  ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_forwarder/))
- [runtimes/staging/datadog_log_trigger](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_log_trigger):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_log_trigger/))
- [runtimes/staging/datadog_log_trigger_frontend](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_log_trigger_frontend)  ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_log_trigger/))
- [runtimes/staging/datadog_alert](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_alert)
  - こちらは「Datadogのアラート設定」の具体的な実装例になります  
    詳細は[こちらのガイド](https://eponas.gitlab.io/epona/guide/how_to/datadog_monitor/)を参照してください

<!-- markdownlint-enable MD013 -->

DatadogおよびPagerDutyはpatternモジュールの適用だけではなく、Web UIからの操作も必要になります。  
これらに関しては、Eponaのモニタリングに関するHow Toドキュメントを参照してください。

- [モニタリング](https://eponas.gitlab.io/epona/guide/activities/monitoring/)
  - [適用pattern・How To](https://eponas.gitlab.io/epona/guide/activities/monitoring/#%E9%81%A9%E7%94%A8patternhow-to)

### Delivery環境にユーザーを作成する

Delivery環境にIAMユーザーを作成します。また、作成したIAMユーザーをRuntime環境へスイッチできる準備をします。

---

- このテーマが支えるアクティビティ
  - [インフラセキュリティ](https://eponas.gitlab.io/epona/guide/activities#%E3%82%A4%E3%83%B3%E3%83%95%E3%83%A9%E3%82%BB%E3%82%AD%E3%83%A5%E3%83%AA%E3%83%86%E3%82%A3)

---

<!-- markdownlint-disable MD013 -->

- [delivery/users](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/users) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/users/))
- [delivery/runtime_instances/staging/bind_role](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/runtime_instances/staging/bind_role) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/bind_role/))

<!-- markdownlint-enable MD013 -->

:information_source: ここで作成するIAMユーザーは、Terraformの実行ユーザーではありません。  
:information_source: 実際にこの環境を運用する、システムのユーザーが使用することを想定したIAMユーザーです。

### Runtime環境にロールを作成する

Runtime環境にロールを作成します。これで、Delivery環境上のIAMユーザーからスイッチが可能になります。

---

- このテーマが支えるアクティビティ
  - [インフラセキュリティ](https://eponas.gitlab.io/epona/guide/activities#%E3%82%A4%E3%83%B3%E3%83%95%E3%83%A9%E3%82%BB%E3%82%AD%E3%83%A5%E3%83%AA%E3%83%86%E3%82%A3)

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/roles](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/roles) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/roles/))

<!-- markdownlint-enable MD013 -->

### Delivery環境の証跡記録とインフラ変更を記録、第三者による不正アクセスを通知する

Delivery環境における、アカウントの証跡やインフラ変更を記録し、ルール違反や第三者による不正アクセスがあった場合は通知できるようにします。

---

- このテーマが支えるアクティビティ
  - [インフラセキュリティ](https://eponas.gitlab.io/epona/guide/activities#%E3%82%A4%E3%83%B3%E3%83%95%E3%83%A9%E3%82%BB%E3%82%AD%E3%83%A5%E3%83%AA%E3%83%86%E3%82%A3)

---

<!-- markdownlint-disable MD013 -->

- [delivery/audit](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/audit) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/audit/))
- [delivery/rule_violation](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/rule_violation) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/rule_violation/))
- [delivery/threat_detection](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/threat_detection) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/threat_detection/))

<!-- markdownlint-enable MD013 -->

### Runtime環境の証跡やインフラ変更を記録、第三者による不正アクセスを通知する

Runtime環境における、アカウントの証跡やインフラ変更を記録し、ルール違反や第三者による不正アクセスがあった場合は通知できるようにします。

---

- このテーマが支えるアクティビティ
  - [インフラセキュリティ](https://eponas.gitlab.io/epona/guide/activities#%E3%82%A4%E3%83%B3%E3%83%95%E3%83%A9%E3%82%BB%E3%82%AD%E3%83%A5%E3%83%AA%E3%83%86%E3%82%A3)

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/audit](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/audit) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/audit/))
- [runtimes/staging/rule_violation](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/rule_violation) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/rule_violation/))
- [runtimes/staging/threat_detection](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/threat_detection) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/threat_detection/))

<!-- markdownlint-enable MD013 -->

:information_source: `rule_violation`, `threat_detection`適用後、これらのログ収集を行う場合は[datadog_log_trigger](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/datadog_log_trigger)の再実行が必要です。依存箇所を修正して再実行してください。

### Runtime環境に仮想セキュアルームを設置する

Runtime環境に、運用中の本番アクセス用に仮想セキュアルームを設置します。

---

- このテーマが支えるアクティビティ
  - [仮想セキュアルーム](https://eponas.gitlab.io/epona/guide/activities#%E4%BB%AE%E6%83%B3%E3%82%BB%E3%82%AD%E3%83%A5%E3%82%A2%E3%83%AB%E3%83%BC%E3%83%A0)

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/operation_network](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/operation_network):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/network/))
- [runtimes/staging/vpc_peering](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/vpc_peering):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/vpc_peering/))
- [runtimes/staging/virtual_secure_room_directory_service](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/virtual_secure_room_directory_service):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_directory_service/))
- [runtimes/staging/virtual_secure_room_workspaces](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/virtual_secure_room_workspaces):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_workspaces/))
- [runtimes/staging/virtual_secure_room_proxy](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/virtual_secure_room_proxy):green_book: ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_proxy/))

<!-- markdownlint-enable MD013 -->

---

仮想セキュアルームから構築済みのリソースへアクセスするためには、セキュリティグループを更新する必要があります。
構築済みAWSリソースのセキュリティグループのインバウンドにWorkSpacesのセキュリティグループを設定するようにモジュールを再適用します。

以下のpatternについて、[Runtime環境にアプリケーションを動作させるための環境を作成する](#runtime環境にアプリケーションを動作させるための環境を作成する)でコメントアウトしていた`*1`部分をコメントインして再適用します。

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/database](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/database) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/database/))
- [runtimes/staging/redis](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/redis) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/redis/))

<!-- markdownlint-enable MD013 -->

### Runtime環境で稼働しているシステムのビジネス指標を可視化する仕組みを構築する

Runtime環境に、ビジネス指標可視化のためのAmazon QuickSightからVPC内のRDSを参照するための仕組みを導入します。

:warning: VPC内のRDSと、Amazon QuickSightで`Enterprise`を選択する必要があります。

---

- このテーマが支えるアクティビティ
  - [モニタリング](https://eponas.gitlab.io/epona/guide/activities#%E3%83%A2%E3%83%8B%E3%82%BF%E3%83%AA%E3%83%B3%E3%82%B0)
  - [コミュニケーション](https://eponas.gitlab.io/epona/guide/activities#%E3%82%B3%E3%83%9F%E3%83%A5%E3%83%8B%E3%82%B1%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3)

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/quicksight_vpc_inbound_source](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/quicksight_vpc_inbound_source) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/quicksight_vpc_inbound_source/))

<!-- markdownlint-enable MD013 -->

---

QuickSightから構築済みのリソースへアクセスするためには、セキュリティグループを更新する必要があります。
構築済みAWSリソースのセキュリティグループのインバウンドにQuickSight用のセキュリティグループを設定するようにモジュールを再適用します。

以下のpatternについて、[Runtime環境にアプリケーションを動作させるための環境を作成する](#runtime環境にアプリケーションを動作させるための環境を作成する)でコメントアウトしていた`*2`部分をコメントインして再適用します。

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/database](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/database) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/database/))

<!-- markdownlint-enable MD013 -->

また、Amazon QuickSightはpatternモジュールの適用だけではなく、Web UIからの操作も必要になります。  
こちらに関しては、QuickSightに関するHow Toドキュメントを参照してください。

- [モニタリング](https://eponas.gitlab.io/epona/guide/activities/monitoring/)
  - [適用pattern・How To](https://eponas.gitlab.io/epona/guide/activities/monitoring/#%E9%81%A9%E7%94%A8patternhow-to)

### Runtime環境にAPIを構築する

Runtime環境に、REST APIアプリケーションを作成・公開します。  
必要に応じて実行してください。

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/dynamo_db](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/dynamo_db)
- [runtimes/staging/lambda_api_gateway/authorizer](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/authorizer) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/lambda/))
- [runtimes/staging/lambda_api_gateway/get_books](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/get_books) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/lambda/))
- [runtimes/staging/lambda_api_gateway/list_books](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/list_books) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/lambda/))
- [runtimes/staging/lambda_api_gateway/post_books](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/post_books) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/lambda/))
- [runtimes/staging/api_gateway](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/api_gateway) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/api_gateway/))
- [runtimes/staging/webacl_api_gateway](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/webacl_api_gateway) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/webacl/))

<!-- markdownlint-enable MD013 -->

applyが完了したら、アプリケーションにアクセスしてください。
このとき、URLのホスト名はapi_gatewayの `custom_domains` で設定した値になります。  
(例：`https://api-example.epona-devops.com/staging/`)  

本手順で構築するアプリケーションは、書籍に関する情報（ID、タイトル、価格など）を登録・確認するような簡易なアプリケーションです。
このアプリケーションには以下のAPIが含まれます。

- 固定レスポンス
  - URL: `https://api-example.epona-devops.com/staging/`
  - メソッド: GET
- 一覧取得
  - URL: `https://api-example.epona-devops.com/staging/books/`
  - メソッド: GET
- 1件登録
  - URL: `https://api-example.epona-devops.com/staging/books/`
  - メソッド: POST
  - Lambdaオーソライザーによる認証あり
- idを指定して1件取得
  - URL: `https://api-example.epona-devops.com/staging/books/{id}`
  - メソッド: GET

1件登録のAPIには、Lambdaオーソライザーが設定されています。  
ヘッダーの`authorizer_token`に設定した文字列で認証する簡易なものです。
APIを実行するためには`authorizer_token`に`allow`を設定してください。

以下はコマンドのサンプルです。

```shell
# 認証成功
$ curl -X POST -H "Content-Type: application/json" -H "authorizer_token: allow" -d '{"id":"101", "title":"book-title101", "price": "4980"}' https://api-example.epona-devops.com/staging/books/
{}

# 認証失敗
$ curl -X POST -H "Content-Type: application/json" -H "authorizer_token: deny" -d '{"id":"101", "title":"book-title101", "price": "4980"}' https://api-example.epona-devops.com/staging/books/
{"Message":"User is not authorized to access this resource with an explicit deny"}
```

### Delivery/Runtime環境にサーバーレスアプリケーションのCI/CDパイプラインを構築する

Delivery/Runtime環境に、CI/CDパイプラインを起動する仕組みを構築します。  
必要に応じて実施してください。

- [Lambda関数をパイプラインを使ってデプロイする](https://gitlab.com/eponas/epona_aws_getting_started/-/blob/master/guide/lambda_pipeline)

### Runtime環境にワークフローを構築する

[`AWS Step Functions`](https://aws.amazon.com/jp/step-functions/)
により、ワークフローを構築します。  
必要に応じて実行してください。

---

<!-- markdownlint-disable MD013 -->

- [runtimes/staging/lambda_step_functions/get_csv](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_step_functions/get_csv) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/lambda/))
  - `runtimes/staging/lambda_step_functions/get_csv/node`ディレクトリで`npm install`を実行してから`terraform apply`を実行
- [runtimes/staging/step_functions](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/step_functions) ([pattern document](https://eponas.gitlab.io/epona/guide/patterns/aws/step_functions/))

<!-- markdownlint-enable MD013 -->

本手順で構築するワークフローは、特定のS3バケットへのファイルアップロードをトリガーとして以下の処理を実行する簡易なワークフローです。

- S3バケットからCSVファイルを取得する
- CSVファイルのデータを1行ずつJSONに変換する
- 変換結果を出力用S3にJSON Linesファイルとして出力する
- ワークフローが正常終了した場合Microsoft Teams、Slackに通知を送信する

```text
inputのCSV例
id,title,price
101,book1-title,3000
201,book2-title,4800

上記inputを変換後のoutput
{"id":"101","title":"book1-title","price":"3000"}
{"id":"201","title":"book2-title","price":"4800"}
```

`runtimes/staging/lambda_step_functions/get_csv`で作成するLambda関数はNode.jsで記述しています。  
`terraform apply`を実行する前に、npmライブラリのインストールが必要です。  
`runtimes/staging/lambda_step_functions/get_csv/node`で`npm install`を実行してください。

applyが完了したら、ワークフローを実行してください。  
`runtimes/staging/lambda_step_functions/get_csv`で作成したS3バケットがトリガーになっています。  
このS3バケット(バケット名：epona-sfn-input-csv)にファイルをアップロードすることでワークフローが起動します。  
テスト用のCSVファイルは `step_functions` パターンを適用したTerraform構成ファイルと同じディレクトリに配置しています。  
以下はaws-cliを使用してトリガーとなるS3バケットへファイルをアップロードするコマンドのサンプルです。

```shell
$ aws s3 cp test_data.csv s3://epona-sfn-input-csv
```

CSVファイルをアップロードすると、作成したワークフローが起動します。ワークフローの状態は、マネジメントコンソールから確認できます。  
ワークフローが正常終了すると、変換後のJSON Linesファイルが別のS3バケットに出力されます。
以下はaws-cliを使用してS3バケットから変換後のJSON Linesファイルをダウンロードするコマンドのサンプルです。

```shell
$ aws s3 cp s3://epona-sfn-output-json/output_data.jsonl ./
```

## この後は

ここまで実践してきた環境構築、学んできたpatternモジュールを使い、自分たちのサービスを作るための環境を構築してみましょう。  
本リポジトリは、その時の参考となるでしょう。
