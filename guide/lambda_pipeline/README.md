# Lambda関数をパイプラインを使ってデプロイする

ここでは、API Gatewayで利用するLambda関数をパイプラインを使ってデプロイする例について解説します。

このサンプルで利用するパターンは6種類です。

| パターン名                                                   | 環境         | 備考            |
| ------------------------------------------------------------ | ------------ | --------------- |
| [lambda pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/lambda/) | Runtime環境  | VPCは利用しない |
| [api_gateway pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/api_gateway/) | Runtime環境  | WAFは利用しない |
| [network pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/network/) | Delivery環境 |                 |
| [ci_pipeline pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/) | Delivery環境 |                 |
| [cd_pipeline_lambda_trigger](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_lambda_trigger/) | Delivery環境 |                 |
| [cd_pipeline_lambda](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_lambda/) | Runtime環境  |                 |

## 概要

このガイド内で作成する`pattern`に関するリソース概要図です。

![概要図](../resources/lambda_pipeline_getting_started.png)

## 事前の準備

- [AWS上でEponaを実行するためのセットアップ](https://eponas.gitlab.io/epona_aws_getting_started/guide/1_environment_setup/)(必須)
- GitLabリポジトリの用意(必須)
  - CI/CDパイプラインを動かすためのファイル等を格納するリポジトリが必要です
  - [こちら](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/ci_pipeline_for_lambda/repository_sample/)にサンプルがあります。このサンプルに含まれているファイルを当該のリポジトリに格納します
- サンプル内の一部パラメータの書き換え(必須)
  - Terraformの実行ロールや、Terraformのstateファイルを保存するS3バケットへのアクセスロール、AWSのアカウントIDを環境に応じて変更します
- パラメータ設定の用意(オプション)
  - 複数のpatternで同じ内容のパラメータを指定する部分があり、サンプルでは以下のように設定されています

| 名称                     | パラメータに指定している値     | 利用pattern(パラメータ名)                                    |
| ------------------------ | ------------------------------ | ------------------------------------------------------------ |
| CDパイプライン名         | epona-lambda-cd-pipeline       | cd_pipeline_lambda(pipeline_name)<br />cd_pipeline_lambda_trigger(pipeline_name) |
| ソースバケット           | epona-lambda-resource | cd_pipeline_lambda(source_bucket_name)<br />cd_pipeline_lambda_trigger(source_bucket_arn ※ARNを指定)<br />ci_pipeline(bucket_name) |
| ソースキー               | settings.zip                   | cd_pipeline_lambda(source_object_key) <br/>cd_pipeline_lambda_trigger(deployment_source_object_key) |
| アーティファクトバケット | epona-lambda-artifacts         | cd_pipeline_lambda(artifact_store_bucket_name)<br />cd_pipeline_lambda_trigger(artifact_store_bucket_arn ※ARNを指定) |

## 構築の流れ

### lambda pattern(Runtime環境)

API Gatewayで利用できるLambda関数のサンプルが[こちら](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/)にあります。今回はこのうち、[get_books](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/get_books)サンプルのみ利用します。

まず、[get_books](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/lambda_api_gateway/get_books)サンプルを利用してリソースを作成します。

Lambda関数内で`Amazon Dynamo DB`にアクセスする部分がありますので、[こちら](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/dynamo_db)のTerraformコードをapplyしてテーブルを作成しておきます。
アイテムは登録されていませんので別途登録します。AWS CLIを利用する場合は以下のようなコマンドで登録可能です。

```shell
$ aws dynamodb put-item --table-name epona-api-example-books --item '{"id":{"S":"book01"}}'
```

登録したアイテムを確認しておきます。

```shell
$ aws dynamodb get-item --table-name epona-api-example-books --key '{"id":{"S":"book01"}}'
{
    "Item": {
        "id": {
            "S": "book01"
        }
    }
}
```

### api_gateway pattern(Runtime環境)

[サンプル](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/api_gateway_lambda_pipeline)を利用してリソースを作成します。

outputに`stage_url`というパラメータがあります。こちらのURLをブラウザでアクセスすると`Hello World!`と表示されます。

```text
 # example
 "stage_url" = "https://xxxxxxxx.execute-api.ap-northeast-1.amazonaws.com/default"
```

<!-- textlint-disable -->

Lambda関数との連携が出来ているか確認する場合は、
`https://xxxxxxxx.execute-api.ap-northeast-1.amazonaws.com/default/books/book01`にアクセスします。
Dynamo DBのテーブルに登録したデータが以下のように表示されていれば成功です。

<!-- textlint-enable -->

```json
{"book":{"id":"book01"}}
```

### network pattern(Delivery環境)

`ci_pipeline pattern`で作成するGitLab Runner用のEC2インスタンスを配置するためのネットワークを作成します。

[サンプル](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/network)を利用してリソースを作成します。

### ci_pipeline pattern(Delivery環境)

GitLab Runner用のEC2インスタンスやソースバケットを作成します。

[サンプル](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/ci_pipeline_for_lambda)を利用してリソースを作成し、
GitLabリポジトリとGitLab Runnerが連携できているか確認します。

詳しい手順は[ガイド](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/)を参照ください。

### cd_pipeline_lambda_trigger pattern(Delivery環境)

[サンプル](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/cd_pipeline_lambda_trigger)を利用してリソースを作成します。

以下のようなoutputが表示されます。これは`cd_pipeline_lambda pattern`で利用します。

```terraform
# example
cd_pipeline_lambda_trigger = {
  "cross_account_access_role_arn" = "arn:aws:iam::777221001925:role/Epona-Lambda-Cd-PipelineAccessRole"
}
```

サンプル内でコメントになっているパラメータがありますが、のちほど`cd_pipeline_lambda pattern`を適用してからパラメータに値を指定しapplyします。

### cd_pipeline_lambda(Runtime環境)

[サンプル](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/cd_pipeline_lambda)を利用してリソースを作成します。

<!-- textlint-disable -->

`cross_account_codepipeline_access_role_arn`に、
`cd_pipeline_lambda_trigger pattern`
のoutputで表示されたロールのARNを指定し、applyを行ってください。

<!-- textlint-enable -->

outputの中に`key_arn`パラメータがあります。これは`cd_pipeline_lambda_trigger pattern`で使用します。

```terraform
# example
"key_arn" = "arn:aws:kms:ap-northeast-1:232105380006:key/9df21734-248f-47eb-a7d2-0bb172073f07"
```

### [修正]cd_pipeline_lambda_trigger pattern(Delivery環境)

`cd_pipeline_lambda pattern`のoutputの`key_arn`を`artifact_store_bucket_encryption_key_arn`に指定し、applyします。

これで、リソースの構築は完了です。

## CI/CDパイプラインの実行

GitLabリポジトリに変更を加えpushをすると、CI/CDパイプラインを実行できます。

変更が想定通りに行われるか確認するために、`epona-example-lambda-get-books`の`default`エイリアスの現在のバージョンを取得しておきます。
マネージドコンソールやAWS CLIで確認できます。

```shell
# aws cli example
$ aws lambda get-alias --function-name epona-example-lambda-get-books --name default
{
    "AliasArn": "arn:aws:lambda:ap-northeast-1:232105380006:function:epona-example-lambda-get-books:default",
    "Name": "default",
    "FunctionVersion": "1",
    "Description": "",
    "RevisionId": "71e94d88-19fe-4550-9605-6906d3a6d697"
}
```

GitLabリポジトリ向けのLambda関数用のサンプルと
Terraformコード側に配置していた[nodeスクリプト](../../runtimes/staging/lambda_api_gateway/get_books/node/index.js)の間には、敢えて以下の差分を作ってあります。

```shell
28c28
<       book: body.Item
---
>       bookid: body.Item
```

コメントを追加するなどLambda関数の実行に影響のない変更を加え、GitLabリポジトリにpushします。

<!-- textlint-disable -->

CI/CDパイプラインの実行が完了した後に、`https://xxxxxxxx.execute-api.ap-northeast-1.amazonaws.com/default/books/book01`にアクセスすると以下のように表示されます。

<!-- textlint-enable -->

```json
{"bookid":{"id":"book01"}}
```

前回実行した際のレスポンスにおけるキーは`book`でしたので、パイプラインによって想定通り新しいLambda関数をデプロイできたことがわかります。

また、`default`エイリアスが向いているバージョンが変わっていることも確認します。

```shell
# aws cli example
$ aws lambda get-alias --function-name epona-example-lambda-get-books --name default
{
    "AliasArn": "arn:aws:lambda:ap-northeast-1:232105380006:function:epona-example-lambda-get-books:default",
    "Name": "default",
    "FunctionVersion": "2",
    "Description": "",
    "RevisionId": "8deaf278-c0ef-41ea-a991-aeaeea57a1bb"
}
```

### CI/CDパイプラインの実行で利用する設定ファイルについて

各ファイルについてのガイドは以下にあります。

#### .gitlab-ci.yml

[GitLab CI/CDによるアプリケーションのビルド・デプロイガイド](https://eponas.gitlab.io/epona/guide/how_to/gitlab_aplication_build_deploy/)

#### buildspec.yml / appspec.yaml

[cd_pipeline_lambdaガイド](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_lambda/)
