# 環境構築

- [概要](#概要)
- [前提条件](#前提条件)
- [セットアップ手順](#セットアップ手順)
  - [Terraform実行環境をセットアップする](#terraform実行環境をセットアップする)
    - [本リポジトリをcloneする](#本リポジトリをcloneする)
    - [導入先に合わせて最低限変更が必要な情報](#導入先に合わせて最低限変更が必要な情報)
    - [Epona AWS patternモジュールのドキュメントを参照する](#epona-aws-patternモジュールのドキュメントを参照する)
    - [Terraform実行ユーザーを作成する](#terraform実行ユーザーを作成する)
  - [example-chatをGitLabに登録する](#example-chatをgitlabに登録する)
- [この後は](#この後は)

本ページでは、Getting Started（本リポジトリ）に含まれるTerraformモジュールを実行するためのセットアップ手順を説明します。

## 概要

ここでは、以下の2つの環境で本リポジトリに含まれるTerraformモジュールを実行するため、環境を整える手順を記載します。

- Delivery環境
- Runtime環境（ステージング環境）

:information_source: 実際の運用では、Runtime環境は用途（テスト環境、本番環境等）に応じて複数必要になるでしょう。  
:information_source: 本リポジトリでは、ステージング環境用としてひとつのRuntime環境を使用します。

また、Runtime環境でアプリケーションを動作させるために必要な、GitLabリポジトリのセットアップについても記載します。

## 前提条件

本ページの記載内容を実施するにあたり、2つのAWSアカウントとGitLabアカウントを用意してください。

- AWS
  - Delivery環境用AWSアカウントおよび、`AdministratorAccess`権限を持つIAMユーザー
  - Runtime環境用（ステージング環境用）AWSアカウントおよび、`AdministratorAccess`権限を持つIAMユーザー
- GitLab
  - `example-chat`ソースコードの格納先

本ページの手順では以下のソフトウェアを使用します。手順を実行する環境にインストールしてください。

- Git
- Terraform
  - Eponaは、Terraform 0.14.10で開発および動作確認を行っています
  - [こちら](https://releases.hashicorp.com/terraform/0.14.10/)から、お使いのプラットフォームに合ったTerraform実行ファイルをダウンロードしてください
- [AWS CLI](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-chap-install.html)

また、Eponaおよび本リポジトリ内のドキュメントが参照できるようにしてください。

## セットアップ手順

### Terraform実行環境をセットアップする

#### 本リポジトリをcloneする

最初に、本リポジトリを`clone`してください。

```shell
$ git clone https://gitlab.com/eponas/epona_aws_getting_started.git
$ cd epona_aws_getting_started
```

以降では、本リポジトリが`clone`されているものとして記載します。

#### 導入先に合わせて最低限変更が必要な情報

`clone`した本リポジトリに含まれているファイルには、以下の情報が固定で記載されています。

| 用途 | clone時の設定値 |
|:-------|:----------------|
| サービス名（任意の名称） | `epona` |
| Delivery環境用AWSアカウント | `777221001925` |
| Runtime環境用（ステージング環境用）AWSアカウント | `232105380006` |
| ドメイン | `epona-devops.com` |

利用の際には、導入先の環境に合わせてこれらの値の修正が必要になります。

#### Epona AWS patternモジュールのドキュメントを参照する

Eponaに含まれる、AWS向けのpatternモジュール用のドキュメントを参照します。

[Epona AWS patternモジュール](https://eponas.gitlab.io/epona/guide/patterns/aws/)

実際の手順へ移る前に、上記ページに記載されている以下の節へ目を通してください。

- [Terraformの実行を支えるリソース](https://eponas.gitlab.io/epona/guide/patterns/aws/#terraform%E3%81%AE%E5%AE%9F%E8%A1%8C%E3%82%92%E6%94%AF%E3%81%88%E3%82%8B%E3%83%AA%E3%82%BD%E3%83%BC%E3%82%B9)

以降では、これら3つのリソースを作成します。

#### Terraform実行ユーザーを作成する

Terraformを実行するための環境を整えます。

Eponaの環境構築手順に沿って、Delivery環境およびRuntime環境を操作するTerraform実行ユーザーを作成してください。

- [Terraform実行環境の構築（詳細）](https://eponas.gitlab.io/epona/guide/patterns/aws/terraform_backend/#%E7%92%B0%E5%A2%83%E6%A7%8B%E7%AF%89%E6%89%8B%E9%A0%86)
- [Terraformの実行](https://eponas.gitlab.io/epona/guide/patterns/aws/terraform_backend/#terraform%E3%81%AE%E5%AE%9F%E8%A1%8C)

---

:information_source:  
本リポジトリが表現している環境の一部を、容易に構築できる[Epona AWS Starter CLI](https://gitlab.com/eponas/epona-aws-starter-cli)も利用できます。

Epona AWS Starter CLIを使用すると、以下の2つの環境構築が容易になります。

- Eponaが想定するTerraform実行環境
- アクティビティ（デプロイメントパイプライン）が実現可能な環境

構築される環境は、本リポジトリと同様に[example-chat](https://github.com/Fintan-contents/example-chat)がデプロイされることを想定しています。

まずはEponaを簡単に試してみたいようでしたら、Epona AWS Starter CLIの利用もご検討いただくとよいでしょう。

---

手順の中でコピーを求められる`setup_terraform_accounts`ディレクトリは、`clone`した本リポジトリに含まれています。

- `setup_terraform_accounts/delivery` … Delivery環境用
- `setup_terraform_accounts/runtimes/staging` … Runtime環境用

環境構築手順を本リポジトリ向けに読み替えるにあたり、以下の内容に注意してください。

- `#FIXME:`の部分を導入先の環境に合わせて修正してください
  - Eponaの環境構築手順ドキュメントのうち、以下の部分に該当します
    - `[任意の名称]`の部分は好みで決定いただいてかまいませんが、S3バケット名等で使用されるためユニークになるように指定してください
    - Runtime環境における`[環境名]`は`Staging`としてください
  - AWSアカウントIDについては、導入先に合わせて変更してください

手順の実施が完了すると、次のリソースが構築されます。

- Delivery環境
  - Terraformを実行するためのIAMユーザー
    - Delivery環境用
    - Runtime環境用
  - Terraformを実行するためのIAMユーザーのクレデンシャル
    - Delivery環境用
    - Runtime環境用
  - Terraformを実行時に必要な権限を与えるIAMロール
  - State管理用のAmazon S3バケット
  - Stateロック用のAmazon DynamoDB
- Runtime環境
  - Terraformを実行時に必要な権限を与えるIAMロール
  - State管理用のAmazon S3バケット
  - Stateロック用のAmazon DynamoDB

:information_source: IAMユーザーは、Delivery環境のみに作成されます。

以降、Terraformの実行には、ここで作成したTerraform実行ユーザーを使用します。  
各環境のTerraform実行ユーザー向けに作成したクレデンシャルは、大切に控えておいてください。

### example-chatをGitLabに登録する

本リポジトリを使って環境を構築し、体感してみるにはアプリケーションが必要です。  
動作させるアプリケーションとしては、`example-chat`を使用します。

- [example-chat](https://github.com/Fintan-contents/example-chat)

デプロイメントパイプラインでは、この`example-chat`をビルド、デプロイします。  
そのためには、GitLab環境に`example-chat`のソースコードを登録する必要があります。

GitLab上に、リポジトリを作成してください。  
特にこだわりがなければ、登録元のリポジトリ名である`example-chat`で良いでしょう。

`example-chat`を、GitHubより`clone`します。

```shell
$ git clone https://github.com/Fintan-contents/example-chat.git
$ cd example-chat
```

`clone`したリポジトリに対して、GitLabリポジトリを`origin`に登録して`push`します。

```shell
$ git remote rename origin fintan-contents
$ git remote add origin [作成したGitLabのリポジトリ名]
$ git push origin
```

ここで`push`したリポジトリは、CIパイプラインで利用します。

以上で、AWSでGetting Startedに含まれるコンテンツを実行できる準備が整いました。

## この後は

[patternモジュールの適用](./2_apply_patterns.md)に進んでください。
