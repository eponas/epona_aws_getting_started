data "terraform_remote_state" "delivery_network" {
  backend = "s3"

  config = {
    bucket         = "epona-delivery-terraform-tfstate"
    key            = "network/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/DeliveryTerraformBackendAccessRole"
  }
}
