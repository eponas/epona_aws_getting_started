# ci_pipeline

本ページでは、`ci_pipeline`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`ci_pipeline`で使用しているpatternモジュールは、以下となります。

- [ci_pipeline pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/)

## terraform applyを実行する前に

### GitLabが利用でき、リポジトリにソースコードが登録されていること

ci_pipelineを実行するには、GitLabを利用できる必要があります。

`example-chat`のソースコードを、ご用意いただいたGitLabリポジトリに登録しておいてください。

- [セットアップ / example-chatをGitLabに登録する](../../guide/1_environment_setup.md#example-chatをgitlabに登録する)

また、GitLabによるCIを実行するためにはリポジトリに`.gitlab-ci.yml`を含める必要があります。

:information_source: `example-chat`には`.gitlab-ci.yml`が含まれているため、そのまま利用できます。

### GitLabのregistration tokenを取得する

`ci_pipeline`を適用するとGitLab Runnerが構築され、GitLabにRunnerとして登録されます。

この条件として、GitLabのregistration tokenが必要になります。

- [GitLab Runnerのregistration tokenについて](https://eponas.gitlab.io/epona/guide/patterns/aws/ci_pipeline/#gitlab-runner%E3%81%AEregistration-token%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)

利用するGitLabのリポジトリの、`Runners`を表示してください。  
registration tokenは、`Runners`の`Set up a specific Runner manually`で確認できます。

> `Runners`は、`Settings` → `CI / CD`から表示できます。

取得したregistration tokenを、`terraform plan`や`terraform apply`時にpatternモジュールの変数に設定してください。

## terraform applyを実行した後に

### ci_pipelineにより構築されたGitLab Runnerが確認できること

`ci_pipeline`適用後は、構築されたGitLab Runnerが`Specific Runners`内に出現していることを確認してください。

### パイプラインに環境変数を設定すること

CIジョブを起動し、[Amazon Elastic Container Registry](https://aws.amazon.com/jp/ecr/)(以下ECR)リポジトリにDockerイメージをPushするためには環境変数の設定が必要になります。

`Settings` → `CI / CD` → `Variables`より、以下の環境変数と値を設定してください。

| 環境変数名 | 設定値 | 例 |
|:-----------|:-------|:----------|
| `ECR_REGISTRY` | `[AWSアカウントID].dkr.ecr.[リージョン].amazonaws.com` | `777221001925.dkr.ecr.ap-northeast-1.amazonaws.com` |
| `EPONA_AWS_DEFAULT_REGION` | リージョン | `ap-northeast-1` |
| `EPONA_FRONTEND_ARCHIVE_FILE_NAME` | S3[^1]にアップロードするフロントエンド・ソースのzipファイル名 | `source.zip` |
| `EPONA_FRONTEND_PUSH_TO_S3_BUCKET_NAME` | フロントエンド・ソースのアップロード先となるS3バケット名 | `epona-static-resource` |
| `EPONA_REACT_APP_BACKEND_BASE_URL` | フロントエンドがバックエンドにリクエストするときに使用するベースURL | `https://chat-example-backend.staging.epona-devops.com` |

## パイプラインを実行する

ここでは[CDパイプラインを構築](../../guide/2_apply_patterns.md#delivery環境にアプリケーションのcdパイプラインを起動する仕組みを構築する)した後で、CIパイプラインを実行する手順について説明します。

### タスク定義の修正

example-chatの中には、[Amazon Elastic Container Service](https://aws.amazon.com/jp/ecs/)にアップロードするタスク定義が含まれています。

- `backend/deploy/taskdef.json`
- `notifier/deploy/taskdef.json`

これらのタスク定義の中には、以下のようにタスクロールとタスク実行ロールが設定されています。

```json
  "taskRoleArn": "arn:aws:iam::232105380006:role/EponaBackendContainerServiceTaskRole",
  "executionRoleArn": "arn:aws:iam::232105380006:role/EponaBackendContainerServiceTaskExecutionRole",
```

ロールのARNにはRuntime環境用のAWSアカウントIDが含まれています。
これらの値は、実際の環境に合わせて書き換えて`master`ブランチにコミットしてください。

### ジョブの起動条件を満たす

example-chatのCIパイプラインには、AWSに各種リソースをデプロイするための以下のジョブが定義されています。

| ジョブ | 説明 | 起動条件 |
|---|---|---|
| `deploy-frontend-to-s3` | frontendのビルド成果物をS3にアップする | `frontend/`以下を修正する |
| `deploy-nginx-backend-to-ecr` | backend用のnginxイメージをECRにプッシュする | `docker/nginx/`以下を修正する |
| `deploy-nginx-notifier-to-ecr` | notifier用のnginxイメージをECRにプッシュする | `docker/nginx/`以下を修正する |
| `deploy-backend-to-ecr` | backendのイメージをECRにプッシュする | `backend/`以下を修正する |
| `deploy-notifier-to-ecr` | notifierのイメージをECRにプッシュする | `notifier/`以下を修正する |
| `deploy-backend-deployment-settings` | backendのデプロイ設定をS3にアップする | `backend/`以下を修正するかつ、`master`へのpush |
| `deploy-notifier-deployment-settings` | notifierのデプロイ設定をS3にアップする | `notifier/`以下を修正するかつ、`master`へのpush |

「起動条件」にあるように、これらのジョブはデプロイ対象のリソースが修正された場合のみ実行されるようになっています。

これらのジョブが実行されるように、以下の条件を満たすファイルを修正して`master`ブランチにコミットしてください。

- `frontend/` 以下のファイル
- `backend/` 以下のファイル
- `notifier/` 以下のファイル
- `docker/nginx/` 以下のファイル

ファイルの種類や変更内容は、アプリケーションの動作を破壊するものでなければ何でも構いません。
`README.md`に1行足したり、`Dockerfile`にコメントを1行足すなどの修正を入れてください。

修正を`master`ブランチにコミットしたら、リモートにpushしてください。
CIパイプラインが起動して、各種リソースがAWSにデプロイされます。

[^1]: [Amazon Simple Storage Service](https://aws.amazon.com/jp/s3/)
