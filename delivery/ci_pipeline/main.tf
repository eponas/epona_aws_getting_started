provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

# Amazon Linux 2 の最新版を取得する
data "aws_ami" "recent_amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "ci_pipeline" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/ci_pipeline?ref=v0.2.6"

  name = "epona-gitlab-runner"
  tags = {
    Owner       = "epona"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  vpc_id = data.terraform_remote_state.delivery_network.outputs.network.vpc_id

  static_resource_buckets = [{
    bucket_name        = "epona-static-resource"
    force_destroy      = false
    runtime_account_id = "232105380006"
  }]

  container_image_repositories = [{
    name                 = "epona-chat-example-backend"
    image_tag_mutability = "MUTABLE"
    repository_policy    = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy     = file("${path.module}/lifecycle_policy.json")
    }, {
    name                 = "epona-chat-example-notifier"
    image_tag_mutability = "MUTABLE"
    repository_policy    = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy     = file("${path.module}/lifecycle_policy.json")
    }, {
    name                 = "epona-backend-nginx"
    image_tag_mutability = "MUTABLE"
    repository_policy    = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy     = file("${path.module}/lifecycle_policy.json")
    }, {
    name                 = "epona-notifier-nginx"
    image_tag_mutability = "MUTABLE"
    repository_policy    = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy     = file("${path.module}/lifecycle_policy.json")
    }, {
    name                 = "epona-virtual-secure-room-proxy"
    image_tag_mutability = "MUTABLE"
    repository_policy    = data.aws_iam_policy_document.cross_account_pull_policy.json
    lifecycle_policy     = file("${path.module}/lifecycle_policy.json")
    }
  ]

  ci_runner_ami                    = data.aws_ami.recent_amazon_linux_2.image_id
  ci_runner_instance_type          = "t3.small"
  ci_runner_root_block_volume_size = "30"
  ci_runner_subnet                 = data.terraform_remote_state.delivery_network.outputs.network.private_subnets[0]

  gitlab_runner_registration_token = var.gitlab_runner_registration_token
}

# Runtime環境からクロスアカウントでPull Accessを可能にするポリシー
# see: https://stackoverflow.com/questions/52914713/aws-ecs-fargate-pull-image-from-a-cross-account-ecr-repo/52934781
data "aws_iam_policy_document" "cross_account_pull_policy" {
  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage"
    ]
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::232105380006:root"]
    }
  }
}
