provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

module "audit" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/audit?ref=v0.2.6"

  trail_name = "epona-delivery-cloudtrail"
  trail_tags = {
    Owner       = "epona"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  bucket_name = "epona-delivery-trail-bucket"

  # cd_pipeline_frontend_triggerでデータイベントを利用するため、S3の全てのデータイベントを記録
  event_data_resources = {
    "s3_event" = {
      type   = "AWS::S3::Object"
      values = ["arn:aws:s3:::"]
    }
  }

  kms_key_alias_name = "alias/epona-trail-encryption-key"
  kms_key_management_arns = [
    "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  ]
}
