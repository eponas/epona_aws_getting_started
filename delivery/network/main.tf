provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.6"

  name = "epona-delivery-network"

  tags = {
    Owner       = "epona"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  cidr_block = "10.250.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.250.1.0/24", "10.250.2.0/24"]
  private_subnets            = ["10.250.3.0/24", "10.250.4.0/24"]
  nat_gateway_deploy_subnets = ["10.250.1.0/24", "10.250.2.0/24"]
}
