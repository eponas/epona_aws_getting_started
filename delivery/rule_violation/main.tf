provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
  region = "ap-northeast-1"
}

module "rule_violation" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/rule_violation?ref=v0.2.6"

  s3_bucket_name = "epona-delivery-rule-violation-bucket"
  tags = {
    Owner       = "epona"
    Environment = "delivery"
    ManagedBy   = "epona"
  }
  function_name           = "eponaDeliveryRuleViolationFunction"
  aws_config_role_name    = "eponaDeliveryRuleViolation"
  notification_topic_name = "epona-delivery-rule-violation-topic"
  notification_type       = "slack" #`teams` または `slack`
  # Getting Startedとして共通で使用できるURLはないため未設定としています。各PJで用意しているIncoming WebhookのURLを設定してください。
  notification_channel = {
    changeDetectionNotice = ""
    ruleViolationNotice   = ""
  }
  # AWS Organizations等によりConfiguration RecorderやDeliverty Channelが既に設定されているAWSアカウントであるため、ここではfalseを設定しています。
  create_config_recorder_delivery_channel = false
  include_grobal_resources                = true
  enable_default_rule                     = true
}
