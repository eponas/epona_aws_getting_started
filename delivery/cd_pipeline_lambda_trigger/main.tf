provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

locals {
  # Runtime環境のアカウントID
  runtime_account_id = "232105380006"
}

data "aws_region" "current" {}

module "cd_pipeline_lambda_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_lambda_trigger?ref=v0.2.6"

  pipeline_name                = "epona-lambda-cd-pipeline"
  source_bucket_arn            = "arn:aws:s3:::epona-lambda-resource"
  deployment_source_object_key = "settings.zip"
  runtime_account_id           = local.runtime_account_id

  # Runtime環境に構築するアーティファクトストアのバケット名
  artifact_store_bucket_arn = "arn:aws:s3:::epona-lambda-artifacts"
  target_event_bus_arn      = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # 以下は Runtime環境上で cd_pipeline_lambda pattern を動かしてから設定する
  #artifact_store_bucket_encryption_key_arn = "arn:aws:kms:${data.aws_region.current.name}:${local.runtime_account_id}:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
