provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

module "threat_detection" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/threat_detection?ref=v0.2.6"

  tags = {
    Owner       = "epona"
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  lambda_function_name = "epona-delivery-guardduty-notification-function"
  lambda_timeout       = 5

  # 通知用Lambda関数を配置するVPC情報
  vpc_id            = data.terraform_remote_state.delivery_network.outputs.network.vpc_id
  lambda_subnet_ids = data.terraform_remote_state.delivery_network.outputs.network.private_subnets
  # 通知先となるendpoint情報
  # Getting Startedとして共通で使用できるURLはないため未設定としています。各PJで用意しているIncoming WebhookのURLを設定してください。
  notification_lambda_config = {
    type = "slack", # `teams` または `slack`
    endpoints = {
      low    = "",
      medium = "",
      high   = ""
    }
  }

  # Amazon GuardDutyが利用できるアカウント、環境の場合、trueを設定してください
  create_guardduty = false
}
