provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

data "aws_caller_identity" "current" {}

module "users" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/users?ref=v0.2.6"

  account_id = data.aws_caller_identity.current.account_id

  users = ["alice", "bob", "charlie", "dave", "ellen", "frank"]

  admins       = ["alice"]
  approvers    = ["bob"]
  costmanagers = ["charlie"]
  developers   = ["dave"]
  operators    = ["ellen"]
  viewers      = ["frank"]

  environment_name = "delivery"
  system_name      = "epona"

  tags = {
    Owner       = "epona"
    Environment = "delivery"
    ManagedBy   = "epona"
  }
  pgp_key = "mQENBF9jXqABCADT6X9vX+eyjT6KXybINKHoV2MPCIxX/4QH6I6Wtg0OKw6gK9jSZp7SQhcssthsITXrkEFaeRPVKzq/DKbA4/GQqMOfpWd2IujCbMs7pTvgI1udQM9EHN0bmJzZZmFSQbLnh6QzzhwPGNTZY73FHrZrMZdR3kimMhJz+k7sa8WQYLvOlGy8bA9u151jH0YlDaC+JAFQGdVeDsW47oXu/NztPR9qHQNKEf3Gg95vH5t0Oy5eJgb8/aF39+Vq6iWLkr9Mx62KM2PcwFNDSN+s+GVfPjhDQi7jI/a7qPVkaIUDhC/b8caXd+tyzeCCpSts8c7XZGyiqiKxb48fcyGgCf7dABEBAAG0HWVwb25hIDxraXJpLnl1aWNoaUB0aXMuY28uanA+iQFUBBMBCAA+FiEE7SINvawVLKIdbAuvY71VogcY0LoFAl9jXqACGwMFCQPCZwAFCwkIBwIGFQoJCAsCBBYCAwECHgECF4AACgkQY71VogcY0LoTlwgAjNK+pAcKScZ5ErEQg9LrELbQg9PCGZ5dk+N5cxY+Y4jVTBF/P78nipQbxV5HsZFaHWt6eSUyPZx1zzClLwO0N4uzayuA20702roOntte1+0Qptg+AfSuQ6tdSnUExPZwrZWWovy9inWD8IyxNLkodNwIRbR3YYrbOFAPoO3ZgE+1ZH7RCjUxlbYZ2wb1dm/sjphK3q+HDigbCm5ZjKrEAJA4rl1SjxUJ8AT0cUX3ouvcfpFOBZ6xhUWrRMaraOBaID0cH88VZYiMKFzOgWrTTWrujz7NMYxgUa8llsdySCusxmfwVGWA0BTKD3giaOuB/iwK6eC6n4Juw+SLGl9PlrkBDQRfY16gAQgAsB/B3YJLozH0PXEGKdibkcf0Zra7nvlFHNCQFe99yn0+1eIop2aAGM2mDqYM+Tw1mF9MbRsmhQGz6cMX+ChY9oOyY+u9JeB453EFc3u+jnAujAoJjof0B8fNhxQGtzcFn9gpU7rvxww4J/w4ih0kQW/7SYglXVP3Djxc9GawgyibU5aZJGuLiWYj8vPOgxzqXg0ikMFrBnXmh19+x3OcdI3eBFmo/qxm/r+2grjDmvMODm/B9/LvFI/LMDHufuAgKrNUC6zYQlFbePVkm0XJnkYmdYnBI0DsKgC9WIpV2e8pzyH/l1YaYb8/kbxSE9lbCvOSo5TOe7mpHiVfYGobjwARAQABiQE8BBgBCAAmFiEE7SINvawVLKIdbAuvY71VogcY0LoFAl9jXqACGwwFCQPCZwAACgkQY71VogcY0Lr1Dwf/QvB6RIW3VO8XWyhdITdqoO6noH8whlh+BhRhwOIgatBR5Ng/cxxLQNp86NEV/EggyImsv1tjRdCFvInQA5Ffim4CsZxNI3j57gfz275okH/l1b7qBfMHzgCthbpTdiSkN2GnU5OBMyqYo7KXsdcfYH4JrCiRznuGEfqPpkMcRm5QctowbBhAtCOK/dd91bDPm6wHo/VTjzUiJIcGnrqi5XM3hB9Z8DcHtKWrIYC5HzbpJ9Tzdccs+xqyjJONPYE9gmpGCQuAjP92mKR0Y8enDZy/xPZw4fFoxoUAH3aEBVJFdlXHbSucBfhh/z5Inz5sjYzRp0xck04IX3hfMzyINg=="
}
