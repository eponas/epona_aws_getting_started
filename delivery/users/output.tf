output "password" {
  description = "ユーザのパスワード"
  value       = module.users.password
}
