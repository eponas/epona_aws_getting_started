// API Path /books/{id}
// API Method GET

console.log('Loading function');

const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  const body = await dynamo
  .get({
    TableName: 'epona-api-example-books',
    Key: {
      id: event.pathParameters.id,
    },
  })
  .promise();
  console.log(body)

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      bookid: body.Item
    }),
  }
};
