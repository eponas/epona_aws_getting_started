provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

# 最新のAmazon Linux 2のAMIを取得するためのデータソース
data "aws_ami" "recent_amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

# ci_pipeline patternの適用 (lambda用）
module "ci_pipeline_for_lambda" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/ci_pipeline?ref=v0.2.6"

  name = "epona-lambda-test-gitlab-runner"

  tags = {
    # 任意のタグ
    Environment = "delivery"
    ManagedBy   = "epona"
  }

  vpc_id = data.terraform_remote_state.delivery_network.outputs.network.vpc_id

  static_resource_buckets = [{
    bucket_name        = "epona-lambda-resource"
    force_destroy      = false
    runtime_account_id = "232105380006"
  }]

  ci_runner_ami                    = data.aws_ami.recent_amazon_linux_2.image_id
  ci_runner_instance_type          = "t3.small"
  ci_runner_root_block_volume_size = "100"
  ci_runner_subnet                 = data.terraform_remote_state.delivery_network.outputs.network.private_subnets[0]

  gitlab_runner_registration_token = var.gitlab_runner_registration_token
}
