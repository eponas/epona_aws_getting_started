provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

module "bind_roles" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/bind_role?ref=v0.2.6"

  account_id = "232105380006"

  admins       = ["alice"]
  approvers    = ["bob"]
  costmanagers = ["charlie"]
  developers   = ["dave"]
  operators    = ["ellen"]
  viewers      = ["frank"]

  environment_name = "staging"
  system_name      = "epona"
}
