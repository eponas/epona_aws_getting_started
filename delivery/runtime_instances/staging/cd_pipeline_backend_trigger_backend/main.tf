provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "232105380006"
}

data "aws_region" "current" {}

module "cd_pipeline_backend_trigger_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.6"

  name                 = "chat-backend-pipeline"
  bucket_name          = "chat-backend-pipeline-source"
  bucket_force_destroy = true
  runtime_account_id   = local.runtime_account_id

  # リポジトリ名と、その ARN およびデプロイ対象タグのマップ
  ecr_repositories = {
    "epona-chat-example-backend" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["epona-chat-example-backend"]
      tags = ["latest"]
    }
    "epona-backend-nginx" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["epona-backend-nginx"]
      tags = ["latest"]
    }
  }

  # CodePipeline が用いる artifact store のバケット ARN
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # 以下は Runtime環境上で cd_pipeline_backend pattern を動かしてから設定する
  artifact_store_bucket_arn                = "arn:aws:s3:::chat-backend-artfct"
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:232105380006:key/29db9bc4-c80e-4ba3-8b70-814ef32cabca"
}
