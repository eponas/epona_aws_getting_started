provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "232105380006"
}

data "aws_region" "current" {}

module "cd_pipeline_frontend_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend_trigger?ref=v0.2.6"

  pipeline_name      = "epona-frontend-cd-pipeline"
  source_bucket_arn  = "arn:aws:s3:::epona-static-resource"
  runtime_account_id = local.runtime_account_id

  # Runtime環境に構築するアーティファクトストアのバケット名
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # 以下は Runtime環境上で cd_pipeline_frontend pattern を動かしてから設定する
  artifact_store_bucket_arn                = "arn:aws:s3:::epona-frontend-artifacts"
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:232105380006:key/7c484df2-0b00-44b6-82f3-d3b4c1cf7bbb"
}
