terraform {
  required_version = "0.14.10"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.37.0"
    }
  }

  backend "s3" {
    bucket         = "epona-delivery-terraform-tfstate"
    key            = "staging/cd_pipeline_frontend_trigger/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/DeliveryTerraformBackendAccessRole"
  }
}
