provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::777221001925:role/TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "232105380006"
}

data "aws_region" "current" {}

module "cd_pipeline_backend_trigger_notifier" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.6"

  # 内部で生成しているデプロイトリガーを伝播するためのEventBridgeのイベント名の文字数制限に引っかかるため notifier -> ntfr と定義
  name                 = "chat-ntfr-pipeline"
  bucket_name          = "chat-ntfr-pipeline-source"
  bucket_force_destroy = true
  runtime_account_id   = local.runtime_account_id

  # リポジトリ名と、その ARN およびデプロイ対象タグのマップ
  ecr_repositories = {
    "epona-chat-example-notifier" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["epona-chat-example-notifier"]
      tags = ["latest"]
    }
    "epona-notifier-nginx" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["epona-notifier-nginx"]
      tags = ["latest"]
    }
  }

  # CodePipeline が用いる artifact store のバケット ARN
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  # 以下は Runtime環境上で cd_pipeline_backend_notifier pattern を動かしてから設定する
  artifact_store_bucket_arn                = "arn:aws:s3:::chat-ntfr-artfct"
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:232105380006:key/5892ad83-3b5c-462d-a717-fad29332b5a6"
}
