# cd_pipeline_backend_trigger_notifier

本ページでは、`cd_pipeline_backend_trigger_notifier`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`cd_pipeline_backend_trigger_notifier`で使用しているpatternモジュールは、以下となります。

- [cd_pipeline_backend_trigger pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/cd_pipeline_backend_trigger/)

## Runtime環境で構築したリソースの情報を反映する

`cd_pipeline_backend_trigger_notifier`は、以下の手順で合計2回`terraform apply`する必要があります。

1. [Delivery環境上にCI/CDパイプラインを構築する](../../../../guide/2_apply_patterns.md#delivery環境上にcicdパイプラインを構築する)
2. [Delivery環境にアプリケーションのCDパイプラインを起動する仕組みを構築する](../../../../guide/2_apply_patterns.md#delivery環境にアプリケーションのcdパイプラインを起動する仕組みを構築する)

1回目はCIパイプラインを構築するために実行します。
2回目はRuntime環境で構築したリソースの情報を反映するために実行します。

ここでは、2回目のRuntime環境で構築したリソースの情報を連携する手順を説明します。

### 構築したリソースの情報を取得する

[Runtime環境にアプリケーションのCDパイプラインを構築する](../../../../guide/2_apply_patterns.md#runtime環境にアプリケーションのcdパイプラインを構築する)で構築したリソースの情報を取得します。

`runtimes/staging/cd_pipeline_backend_notifier`に移動して、`terraform output`コマンドを実行してください。

```shell
$ cd runtimes/staging/cd_pipeline_backend_notifier

$ terraform output
```

以下の出力値をメモしてください。

| 出力値 | 説明 |
|---|---|
| `artifact_store_bucket_arn` | Artifact store用bucketのARN |
| `kms_keys`の`key_arn` | 作成されたKMSの鍵のARN |

### リソースの情報を設定する

<!-- markdownlint-disable MD013 -->

メモしたリソースの情報を`delivery/runtime_instances/staging/cd_pipeline_backend_trigger_notifier/main.tf`に反映します。  
以下の要領でパラメータを設定してください。

<!-- markdownlint-enable MD013 -->

| 設定先のパラメータ | 設定する値 |
|---|---|
| `artifact_store_bucket_arn` | Artifact store用bucketのARN |
| `artifact_store_bucket_encryption_key_arn` | 作成されたKMSの鍵のARN |

設定が完了したら、`terraform apply`を実行してください。
