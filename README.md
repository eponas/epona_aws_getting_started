# 概要

- [概要](#概要)
  - [About Getting Started](#about-getting-started)
    - [Repository Contents](#repository-contents)
      - [Terraform Modules](#terraform-modules)
      - [Guide](#guide)
      - [External Repository Contents](#external-repository-contents)
    - [System Requirements](#system-requirements)
  - [Set up](#set-up)

<!-- from, Epona README.md -->
[Epona](https://eponas.gitlab.io/epona/)は、新たなビジネスアイデアをサービス化する際に必要な開発環境、サービスを実際に稼働させる環境、
そしてこれらの環境へセキュアにアクセスできる手段をクラウド上に構築するためのツールキットです。

本リポジトリは、次の目的を達成するために作成されています。

- [Amazon Web Services](https://aws.amazon.com/jp/) (AWS)へのEponaの適用例を示す
- Eponaでどのようなことが実現できるのか、アプリケーション例と共に示す
- サービス固有の要件を満たすためのEponaの拡張例を示す

Eponaを初めて利用するユーザーに対して、Eponaとはどのようなものなのかを示し、その利用方法を学んでもらうためのコンテンツが含まれています。  
また、Eponaの使用例としてのリファレンスともなるでしょう。

本リポジトリのガイドに沿って進めることで、サービスが動作する環境と、コンテナリポジトリ等を配置する以下の環境が構成されます。

![Getting Started全体像](./guide/resources/getting_started_overview.png)

:information_source: 描画スペース上、全ての構成要素を図示しているわけではありません。

実際に環境を構築してEponaに対する理解を深め、自分たちのサービスを作るためステップとして本リポジトリを使用してください。

---

:information_source:  
本リポジトリが表現している環境の一部を、容易に構築できる[Epona AWS Starter CLI](https://gitlab.com/eponas/epona-aws-starter-cli)も利用できます。

Epona AWS Starter CLIを使用すると、以下の2つの環境構築が容易になります。

- Eponaが想定するTerraform実行環境
- アクティビティ（デプロイメントパイプライン）が実現可能な環境

構築される環境は、本リポジトリと同様に[example-chat](https://github.com/Fintan-contents/example-chat)がデプロイされることを想定しています。

本リポジトリは、Eponaを使う際の参考としたり、理解度を高めることを目的としたリポジトリです。

まずはEponaを簡単に試してみたいようでしたら、Epona AWS Starter CLIの利用もご検討いただくとよいでしょう。

---

<!-- TOC -->

## About Getting Started

### Repository Contents

本リポジトリは、Eponaを利用するTerraformモジュール、およびガイドで構成されています。

#### Terraform Modules

Eponaの本体は、[Terraformモジュール](https://eponas.gitlab.io/epona/guide/patterns/)として提供されています。

:information_source: Eponaが提供し、利用者が使用するTerraformモジュールを、[pattern](https://eponas.gitlab.io/epona/guide/patterns#pattern%E3%83%A2%E3%82%B8%E3%83%A5%E3%83%BC%E3%83%AB)モジュールと呼んでいます。

本リポジトリには、Eponaのpatternモジュールを適用したTerraformの構成ファイル（ルートモジュール）が含まれています。

環境やモジュールごとにディレクトリが分かれており、利用者がガイドに沿って適用していくことで、環境が構築できます。

:information_source: EponaではDelivery環境、Runtime環境と呼ぶ2種類の環境を使用します。

本リポジトリに含まれているTerraformの構成ファイルを見ることでpatternモジュールの利用方法を学ぶことができ、適用することで実感を得られるでしょう。

#### Guide

本リポジトリには、以下の内容を記載しているガイドが含まれています。

- 本リポジトリの内容を実践するにあたっての、セットアップ手順
- Terraformモジュールの実行順の説明
- 各モジュールで構築される環境の解説

#### External Repository Contents

本リポジトリに含まれるものは、Terraformの構成ファイル（ルートモジュール）とガイドです。  
Epona自身はTerraformモジュールであるため、実際に動作するアプリケーションを含みません。

しかし、Eponaを使用して構築した環境上でアプリケーションを動作させ、実際にユーザーとしても環境を使ってみる方がEponaに対する理解が進むでしょう。

本リポジトリでは、この目的のために次のアプリケーションを使用します。

- [example-chat](https://github.com/Fintan-contents/example-chat)

`example-chat`は、サーバーサイドをコンテナ環境上、フロントエンドをSPAとして動作させることを意識したチャットアプリケーションです。  
Eponaがサポートするアプリケーションアーキテクチャーとも合致するため、Eponaへの理解の助けとなるでしょう。

また、`example-chat`はアプリケーションの要件として、Eponaのpatternモジュールとして提供していないAWSリソースを必要とします。  
本リポジトリでは、このようにEponaがモジュール提供していない場合にどうするのか、その拡張例も含まれています。

### System Requirements

本リポジトリに含まれるTerraformモジュールを実行するには、以下の条件を満たしておく必要があります。

- クラウドプロバイダーおよびSaaSを利用可能となっていること
  - クラウドプロバイダー
    - [AWS](https://aws.amazon.com/jp/)
      - 2つのAWSアカウント（Delivery環境 × 1、Runtime環境 × 1）が必要です
      - Amazon SESで検証済みのEメールアドレスを保持していること
      - Amazon Route 53を使った独自ドメインを取得していること
  - SaaS
    - [GitLab.com](https://gitlab.com/)
      - SaaS以外に利用できるGitLab環境がある場合は、そちらでも構いません
    - [Slack](https://slack.com/intl/ja-jp/)または[Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-365/microsoft-teams/group-chat-software)
    - [Datadog](https://www.datadoghq.com/ja/)
    - [PagerDuty](https://ja.pagerduty.com/)
- 以下のソフトウェアがインストールされていること
  - Git
  - Terraform
    - Eponaは、Terraform 0.14.10で開発および動作確認を行っています
    - [こちら](https://releases.hashicorp.com/terraform/0.14.10/)から、お使いのプラットフォームに合ったTerraform実行ファイルをダウンロードしてください
  - [AWS CLI](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-chap-install.html)
  
:information_source: Datadog、PagerDutyがなくてもある程度環境構築はできますが、モニタリングやインシデント管理等に支障が出ます。

## Set up

以下のドキュメントに沿って、Eponaを使用した環境を構築してください。

1. [環境構築](guide/1_environment_setup.md)
1. [patternモジュールの適用](guide/2_apply_patterns.md)

本リポジトリを使用して、Eponaがどのようなものかイメージを掴めたら、次は自分たちが開発するサービスを動作させる環境を作ってみましょう。  
[Epona](https://eponas.gitlab.io/epona/)のドキュメントを読み、本リポジトリの内容をサンプルとして活用してください。
