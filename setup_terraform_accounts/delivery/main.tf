provider "aws" {
}

module "delivery" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/delivery?ref=v0.2.6"

  name          = "epona" # FIXME:任意の名称に修正してください
  force_destroy = false
  runtime_accounts = {
    "Staging" = "232105380006" # FIXME:導入先の環境に合わせて修正してください
  }
}

module "cross_account_assume_role" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/cross_account_assume_policy?ref=v0.2.6"

  execution_role_map = {
    "Staging" = "arn:aws:iam::232105380006:role/TerraformExecutionRole" # FIXME:導入先の環境に合わせて修正してください
  }
  terraformer_users = module.delivery.terraformer_users
}
