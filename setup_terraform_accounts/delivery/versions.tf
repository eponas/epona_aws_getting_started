terraform {
  required_version = "0.14.10"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.37.0"
    }
  }

  backend "s3" {
  }
}
