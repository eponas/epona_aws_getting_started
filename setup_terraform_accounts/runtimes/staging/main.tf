provider "aws" {
}

# Runtime環境上でのロール
module "terraformer_execution" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/terraform_execution?ref=v0.2.6"

  principals = ["arn:aws:iam::777221001925:user/StagingTerraformer"] # FIXME:導入先の環境に合わせて修正してください
}

module "backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/backend?ref=v0.2.6"

  name          = "epona" # FIXME:任意の名称に修正してください
  environment   = "Staging"
  force_destroy = false
}
