from setuptools import setup

setup(
    name='exclude_plugin_getting_started',
    packages=['exclude_plugin_getting_started'],
    entry_points={
        'mkdocs.plugins': [
            'exclude_plugin_getting_started = exclude_plugin_getting_started.exclude:ExcludePlugin',
        ]
    },
)