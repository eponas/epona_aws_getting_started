from mkdocs.plugins import BasePlugin

# MkDocs は、 docs_dir で指定されたディレクトリ以下の全てのサブディレクトリの Markdown ファイルが処理対象になる。
# https://github.com/mkdocs/mkdocs/issues/1665
# 上記 Issue によると、一部のディレクトリを処理対象から除外するためにはプラグインが必要とのことなので、本プラグインが作成された。
# プラグインの作り方は下記を参照。
# https://mkdocs.nakaken88.com/plugin/create-your-plugin/
class ExcludePlugin(BasePlugin):
    def on_files(self, files, config):
        # guide/overrides などがスラッシュとバックスラッシュの2種類用意されているのは、
        # Windows 上ではパス区切り文字がバックスラッシュで処理されるため。
        files._files = [file for file in files._files if not file.src_path.startswith((
            'ci_container_images',
            'setup_terraform_accounts',
            'guide/overrides',
            'guide\\overrides'
        ))]

        return files
