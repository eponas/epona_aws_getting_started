# Change Log

<!--

## [Unreleased]

### Breaking Changes

- aaa(!MergeRequestNo)

### Features

- bbb(MergeRequestNo)

### Enhancements

- ccc(MergeRequestNo)

### Bug Fixes

- ddd(MergeRequestNo)

-->

## 0.2.6 - 2021/09/30

### Features

- [Epona 0.2.6](https://gitlab.com/eponas/epona/-/releases/v0.2.6)適用
- lambda関数向けCDパイプラインの適用(!132)

### Bug Fixes

- step_functionsで設定する内容にlog_group_nameを追加(!134)

## 0.2.5 - 2021/08/31

### Features

- [Epona 0.2.5](https://gitlab.com/eponas/epona/-/releases/v0.2.5)適用
- api_gateway patternの適用(!126)
- step_functions patternの適用(!127)

## 0.2.4 - 2021/05/31

### Features

- [Epona 0.2.4](https://gitlab.com/eponas/epona/-/releases/v0.2.4)適用

## 0.2.3 - 2021/04/28

### Features

- [Epona 0.2.3](https://gitlab.com/eponas/epona/-/releases/v0.2.3)適用

## 0.2.2 - 2021/03/31

### Breaking Changes

- cacheable_frontend
  - モジュール定義からのttl変数の削除に対応(!110)

### Enhancements

- datadog_log_trigger
  - READMEにログフィルターについて追記(!112)
- misc
  - 簡易な環境構築の方法として、Epona AWS Starter CLIを紹介(!114)

### Bug Fixes

- Terraformのダウンロードリンクを、Eponaで動作確認しているバージョンに合わせるように修正(!111)

## 0.2.1 - 2021/01/21

### Features

- [Epona 0.2.1](https://gitlab.com/eponas/epona/-/releases/v0.2.1)適用
- cacheable_frontend
  - outputにoriginバケットのパスを追加(!102)
- cd_pipeline_frontend
  - cacheable_frontendの依存関係を追加(!102)
- setup_terraform_accounts
  - FIXMEコメントを修正（!103）

### Bug Fixes

- 「概要」の表題を修正(!104)
- misc
  - Copyright表記の追加、Author欄削除(!105)

## 0.2.0 - 2020-12-23

### Features

<!-- textlint-disable -->
- [Epona 0.2.0](https://gitlab.com/eponas/epona/-/releases/v0.2.0)適用
  - virtual_secure_room_workspaces patternの適用 (!61)
  - vpc_peering patternの適用 (!62)
  - virtual_secure_room_proxy patternの適用 (!72)
  - WorkSpacesからAWSリソースを参照するための実装を追加 (!75)
  - quicksight_vpc_inbound_source patternの適用 (!72)
  - GitLab Pages対応(!85)
  - 特定タグを付与したEC2のみモニタリングする(!93)
<!-- textlint-enable -->
- [Epona 0.1.3](https://gitlab.com/eponas/epona/-/releases/v0.1.3)適用継続
  - 下記patternをDataDogと連携するように、datadog_log_trigger patternを修正
    - threat_detection pattern (!60)
    - rule_violation pattern (!60)
    - webacl pattern(frontend用) (!60)
    - cd_pipeline_frontend pattern (!83)
- [Epona 0.1.2](https://gitlab.com/eponas/epona/-/releases/v0.1.2)適用継続
  - threat_detection pattern add(!60)
- misc
  - MkDocsでHTMLドキュメントを出力する仕組みを追加(!49)
  - GitLab Pagesでディレクトリのリンクが見れるようにURLを変更(!88, !90)
  - LICENSEファイル追加(!89)
  - Eponaのドキュメント参照をGitLab Pagesへ変更(!91)
  - dead linkの修正(!92)
  - GitLab Pagesのドキュメントライセンスを`CC BY-SA 4.0`に変更(!100)

### Bug Fixes

- GitLab CI/CD Pipelineで設定する変数に過不足があったので修正(!67)
- t2.microでは性能が不足してGitLabのPipelineが終了しないのでt3.smallを使用するように修正(!68)
- Datadogとの連携で適用するパターン一覧にdatadog_log_triggerが無かったので追加(!69)
- cacheable_frontendの前にwebacl_frontend patternの適用が必要だが実施手順から漏れていたので追加(!66)
- redis patternに設定が不足しているパラメータが２つ存在したので追加(!65)
- 初回環境構築で実施しなければならない手順にクレデンシャルを作る手順が漏れていたので追記(!64)
- 環境構築手順の通りに、作成したTerraformerの情報が出力されるようoutput.tfを追加(!70)
- sourceにつけられたFIXMEコメントを削除(!71)
- Getting Startedのテストで発見した軽微な誤記の修正(!73)
- Artifact Store用のS3バケットのARNをci_pipelineのtriggerに連携する手順を追記(!74, !76)
- datadog_alertの説明がなかったため適用パターン一覧に追記(!80)
- datadog_log_trigger初回適用時は依存インスタンスの記述をコメントアウトするようガイドを追加(!79)
- SES用IAMユーザーを作成するパターンの追加とparameter_storeの入力変数ファイルの作り方を追加(!76)
- CIパイプラインを実行してアプリケーションをデプロイする方法のガイドを追加(!81)
- 仮想セキュアルームの構築に関する補足のガイドを追加(!84)
- audit patternのdata_event_sourceのデフォルト値変更に伴う修正(!94)
- redis patternで利用されなくなったパラメータを削除(!96)
- cacheable_frontendの不要な設定を削除(!97)

## 0.1.3 - 2020-12-02

### Breaking Changes

- [Epona 0.1.3](https://gitlab.com/eponas/epona/-/releases/v0.1.3)適用
  - public_traffic_container_service patternで初回デプロイするコンテナイメージをhttp-echoに変更(!43)

### Features

- [Epona 0.1.3](https://gitlab.com/eponas/epona/-/releases/v0.1.3)適用
  - datadogアラートの追加 (!34)
- [Epona 0.1.2](https://gitlab.com/eponas/epona/-/releases/v0.1.2)適用継続
  - datadog_log_trigger pattern add (!42)

### Enhancements

- [Epona 0.1.3](https://gitlab.com/eponas/epona/-/releases/v0.1.3)適用
  - public_traffic_container_server patternにDatadog Agentを導入(!38)
    - backend、notifierともにDatadog Agentコンテナを追加し、コンテナのメトリクスをDatadogへ送信
- [Epona 0.1.2](https://gitlab.com/eponas/epona/-/releases/v0.1.2)適用継続
  - public_traffic_container_server patternにAWS FireLensの導入(!33)
    - backend、notifierともにFluent Bitコンテナを追加し、コンテナのログをCloudWatch Logsへ送信
- 導入およびセットアップドキュメントの追加(!44)
- backend、notifierそれぞれの前段にリバースプロキシを配置(!54)
- misc
  - AWS Providerのバージョンアップ（3.13.0 → 3.18.0）(!39, !40, !46, !50)
  - Datadog Providerのバージョンアップ（2.15.0 → 2.17.0）(!41, !51)
  - tflintを0.20.3から0.21.0へバージョンアップ (!52)

### Bug Fixes

- AWS FargateのServiceに付与しているSecurity Groupの許可範囲が広すぎたため、制限する (!48)
- ALBのデフォルトのセキュリティポリシーを`ELBSecurityPolicy-TLS-1-2-Ext-2018-06`に変更し、セキュリティ強度を向上(!53)

## 0.1.2 - 2020-11-09

### Breaking Changes

- [Epona 0.1.2](https://gitlab.com/eponas/epona/-/releases/v0.1.2)適用
  - cacheable_frontend patternの最新コードに更新(!24)
    - OriginのS3バケットとCloudFront関連のリソースのリージョンを分離

### Features

- [Epona 0.1.2](https://gitlab.com/eponas/epona/-/releases/v0.1.2)適用
  - virtual_secure_room_directory_service patternの適用 (!10)
  - rule_violation patternの適用 (!19)
  - datadog_integration patternの適用(!23)
  - datadog_forwarder patternの適用(!26)
  - webacl patternのフロントエンドへの適用(!32)

### Enhancements

- READMEを追加 (!31)
- リファレンスアプリケーションの新アーキテクチャを反映 (!29)
- [Epona 0.1.2](https://gitlab.com/eponas/epona/-/releases/v0.1.2)適用
  - cd_pipeline_frontend patternの最新コードを反映(!25)
    - CodeBuildでS3バケットへデプロイすることで、古いデプロイファイルを削除するように変更
    - デプロイ後にCloudFrontのキャッシュを削除する機能を有効化
- misc
  - AWS Providerのバージョンアップ（3.6.0 → 3.13.0）(!12, !15, !16, !17, !21, !27)
  - Terraformを0.13.5にアップデート (!11, !14, !20)
  - tflintを0.19.1から0.20.3にアップデート (!13, !18)
  - Datadog Providerのバージョンアップ（2.13.0 → 2.15.0）(!23, !28)

### Bug Fixes

- GitLab CIの修正(!30)

## 0.1.1 - 2020-09-18

### Features

- [Epona 0.1.1](https://gitlab.com/eponas/epona/-/releases/v0.1.1)適用 (!8)
  - bind-role patternの適用
  - users patternの適用
  - roles patternの適用
  - cd_pipeline_frontend_trigger patternの適用
  - cd_pipeline_frontend patternの適用
  - database patternにロギング、モニタリングに関する設定を追加

### Enhancements

- misc
  - Providerのバージョン指定の記述スタイルを変更（`provider.version`から`terraform.required_providers`へ）(!5)
  - AWS Providerのバージョンアップ（3.0.0 → 3.6.0）(!6, !7)
  - tflintのバージョンアップ（0.18.0 → 0.19.1）(!5)

## 0.1.0 - 2020-08-19

### Features

- [Epona 0.1.0](https://gitlab.com/eponas/epona/-/releases/v0.1.0)適用
