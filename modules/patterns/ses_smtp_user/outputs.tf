output "access_key_id" {
  value       = aws_iam_access_key.ses_smtp_key.id
  description = "SMTP認証時に、ユーザー名として使用するアクセスキーID"
  # 本来はsensitive指定を行うべきだが、読者によるGetting Startedの環境構築を簡易にするためsensitive指定を外している
  # sensitive = true
}

output "secret" {
  value       = aws_iam_access_key.ses_smtp_key.secret
  description = "シークレットアクセスキー"
  # 本来はsensitive指定を行うべきだが、読者によるGetting Startedの環境構築を簡易にするためsensitive指定を外している
  # sensitive = true
}

output "smtp_password_v4" {
  value       = aws_iam_access_key.ses_smtp_key.ses_smtp_password_v4
  description = "SMTP認証時にパスワードとして使用する、シークレットアクセスキーを署名バージョン4アルゴリズムで変換した値"
  # 本来はsensitive指定を行うべきだが、読者によるGetting Startedの環境構築を簡易にするためsensitive指定を外している
  # sensitive = true
}
