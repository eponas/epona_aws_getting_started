provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

variable "api_key" {
  type = string
}

variable "app_key" {
  type = string
}

provider "datadog" {
  api_key = var.api_key
  app_key = var.app_key
}

# 本モニター設定は、us-east-1 リージョンでのDNSクエリの数が1でwarning、2以上でcriticalを指定したチャネルに発報する例です。
# あくまで具体的なサンプルとしてご参照ください。通常は DNS クエリ数のような項目を監視することは少ないです。
# 
# aws.route53.dnsqueries については、https://docs.datadoghq.com/ja/integrations/amazon_route53/#%E5%8F%8E%E9%9B%86%E3%83%87%E3%83%BC%E3%82%BF を参照ください。

resource "datadog_monitor" "metric_alert_route53_dnsqueries" {
  name = "route53_dnsqueries"
  type = "metric alert"

  # 事前にSlackインテグレーションでruntime-monitorチャンネルに通知する設定が必要です。
  # Slackインテグレーションの設定方法は、https://docs.datadoghq.com/ja/integrations/slack/?tab=slackwebhookeu を参照ください。
  message = "`aws.route53.dnsqueries` was detected. this message setup by terraform : @slack-runtime-monitor"
  query   = "sum(last_5m):avg:aws.route53.dnsqueries{hostedzoneid:z10456113p4voj60j2cu7,region:us-east-1}.as_count() >= 2"

  # 1でwarning、2以上でcriticalが発報されます。
  monitor_thresholds {
    warning  = 1
    critical = 2
  }

  notify_no_data    = false
  renotify_interval = 60

  notify_audit = false
  timeout_h    = 60
  include_tags = true

  lifecycle {
    ignore_changes = [silenced]
  }

  tags = ["terraform", "epona", "runtime", "staging"]
}

# 本モニター設定は、RDSのログに`database system is shut down`が出力されるとcritiaclを指定したチャネルに発報する例です。
# warningは0より大きくcriticalより小さい値を指定する必要があるため0.1を指定しています。実際には0か1以上しかあり得ないため、このサンプルだとwarningは発報されません。
# また、このサンプルで使っている@msgというファセットはdatadogのデフォルトには無いため、あらかじめログの本文をmsgに格納するパース設定をしておく必要があります。

resource "datadog_monitor" "log_alert_rds_shutdown" {
  name = "rds_shutdown"
  type = "log alert"

  # 事前にMicrosoft Teamsインテグレーションでruntime-monitorチャンネルに通知する設定が必要です。
  # Microsoft Teamsインテグレーションの設定方法は、https://docs.datadoghq.com/ja/integrations/microsoft_teams/ を参照ください。
  message = "`database system is shut down` has been detected @teams-runtime-monitor"
  query   = "logs(\"service:rds @msg:\\\"  database system is shut down\\\"\").index(\"*\").rollup(\"count\").last(\"5m\") >= 1"

  # 1以上でcriticalが発報されます。
  monitor_thresholds {
    warning  = 0.1
    critical = 1
  }

  enable_logs_sample = true

  lifecycle {
    ignore_changes = [silenced]
  }

  tags = ["terraform", "epona", "runtime", "staging"]
}
