provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "virtual_secure_room_proxy" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/virtual_secure_room_proxy?ref=v0.2.6"

  name_prefix       = "Epona"
  vpc_id            = data.terraform_remote_state.staging_operation_network.outputs.network.vpc_id
  container_subnets = data.terraform_remote_state.staging_operation_network.outputs.network.private_subnets

  service_discovery_namespace_name = "epona.internal"
  service_discovery_service_name   = "proxy"

  repository_url        = "777221001925.dkr.ecr.ap-northeast-1.amazonaws.com/epona-virtual-secure-room-proxy"
  proxy_image_tag       = "v1.0"
  container_task_cpu    = "256"
  container_task_memory = "512"

  default_ecs_task_execution_iam_policy_name = "EponaProxyContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "EponaProxyContainerServiceTaskExecutionRole"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}

