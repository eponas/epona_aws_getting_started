# virtual_secure_room_proxy

本ページでは、`virtual_secure_room_proxy`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`virtual_secure_room_proxy`で使用しているpatternモジュールは、以下となります。

- [virtual_secure_room_proxy pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_proxy/)

## terraform applyを実行する前に

### コンテナイメージのデプロイ

本モジュールを実行する前に、プロキシのコンテナイメージを
[Amazon Elastic Container Registry](https://aws.amazon.com/jp/ecr/)
(以下ECR)にデプロイしている必要があります。

コンテナイメージのビルドやデプロイの手順については、`virtual_secure_room_proxy pattern`の以下のガイドを参照してください。

- [コンテナイメージのビルドについて](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_proxy/#%E3%82%B3%E3%83%B3%E3%83%86%E3%83%8A%E3%82%A4%E3%83%A1%E3%83%BC%E3%82%B8%E3%81%AE%E3%83%93%E3%83%AB%E3%83%89%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)
- [コンテナイメージのPushとコンテナデプロイについて](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_proxy/#%E3%82%B3%E3%83%B3%E3%83%86%E3%83%8A%E3%82%A4%E3%83%A1%E3%83%BC%E3%82%B8%E3%81%AEpush%E3%81%A8%E3%82%B3%E3%83%B3%E3%83%86%E3%83%8A%E3%83%87%E3%83%97%E3%83%AD%E3%82%A4%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)

なお、デプロイ先となるECRリポジトリは、[delivery/ci_pipeline](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/delivery/ci_pipeline)モジュールによって作成されています。
設定を変更していない場合、リポジトリ名は`epona-virtual-secure-room-proxy`になります。

## terraform applyを実行した後に

### DNSフォワーダーを設定する

本モジュールを実行し終えたら、次にDNSフォワーダーを設定する必要があります。
具体的な手順は、[virtual_secure_room_directory_service patternのガイド](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_directory_service/#dns%E3%83%95%E3%82%A9%E3%83%AF%E3%83%BC%E3%83%80%E3%83%BC%E3%81%AE%E8%A8%AD%E5%AE%9A%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)を参照してください。

### ブラウザのプロキシ設定

Workspacesのブラウザでプロキシを設定します。
具体的な手順は、[virtual_secure_room_proxy patternのガイド](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_proxy/#proxy%E3%81%AE%E8%A8%AD%E5%AE%9A%E6%96%B9%E6%B3%95%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)を参照してください。
