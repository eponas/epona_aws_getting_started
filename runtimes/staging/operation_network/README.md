# operation_network

本ページでは、Staging環境の`operation_network`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`operation_network`で使用しているpatternモジュールは、以下となります。

- [network pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/network/)

## terraform applyを実行する前に

本モジュールには、この後の手順で実施する以下のモジュールに依存している箇所があります。

- `runtimes/staging/vpc_peering`

したがって、初回適用時は依存部分をコメントアウトして実行する必要があります。

依存部分を含むファイルは、`instance_dependencies.tf`と`main.tf`です。  
それぞれ`vpc_peering pattern実行前は以下～`と書かれた部分をコメントアウトしてから`terraform apply`を実行してください。

## VPCピアリング後の再適用

再適用は、[Runtime環境に仮想セキュアルームを設置する](../../../guide/2_apply_patterns.md#runtime環境に仮想セキュアルームを設置する)の中の、`vpc_peering`の後に実施します。

前述の手順でコメントアウトした部分をコメントインし、`terraform apply`を実行してください。
