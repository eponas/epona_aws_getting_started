# peering関連のremote_state参照はvpc_peering pattern実行後に設定する。
# vpc_peering pattern実行前は以下remote_state参照をコメントアウトにする。
data "terraform_remote_state" "vpc_peering" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "vpc_peering/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}
