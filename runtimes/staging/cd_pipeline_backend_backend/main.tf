provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "cd_pipeline_backend_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.6"

  delivery_account_id = "777221001925"
  ecr_repositories = {
    epona-chat-example-backend = {
      tag            = "latest" # リリース対象タグ
      artifact_name  = "chat"
      container_name = "IMAGE1_NAME"
    }
    epona-backend-nginx = {
      tag            = "latest" # リリース対象タグ
      artifact_name  = "nginx"
      container_name = "IMAGE2_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_trigger_backend の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::777221001925:role/Chat-Backend-PipelineAccessRole"
  # cd_pipeline_backend_trigger_backend の bucket_name と同じ名前を指定する
  source_bucket_name = "chat-backend-pipeline-source"

  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service.ecs_service_name

  pipeline_name              = "chat-backend-pipeline"
  artifact_store_bucket_name = "chat-backend-artfct"

  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service.load_balancer_target_group_names

  codedeploy_app_name         = "epona-chat-backend"
  deployment_group_name_ecs   = "epona-chat-backend-group"
  deployment_require_approval = false
}
