provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "cd_pipeline_lambda" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_lambda?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
  delivery_account_id = "777221001925"

  pipeline_name = "epona-lambda-cd-pipeline"

  artifact_store_bucket_name = "epona-lambda-artifacts"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_lambda_trigger の output として出力される
  # cross_account_codepipeline_access_role_arn = "arn:aws:iam::777221001925:role/Epona-Lambda-Cd-PipelineAccessRole"

  # ci_pipeline の bucket_name と同じ名前を指定する
  source_bucket_name = "epona-lambda-resource"
  source_object_key  = "settings.zip"

  codedeploy_deployment_group_name = "deployment_group-lambda-epona"
  codedeploy_name                  = "codedeploy-lambda-epona"

}
