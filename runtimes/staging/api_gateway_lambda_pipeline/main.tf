provider "aws" {
  alias = "default_provider"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias  = "cloudfront_provider"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "api_gateway_lambda_pipeline" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/api_gateway?ref=v0.2.6"

  providers = {
    aws.default_provider    = aws.default_provider
    aws.cloudfront_provider = aws.cloudfront_provider
  }

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  api_gateway_settings = {
    api_name        = "epona-lambda-pipeline-example"
    api_description = "Epona Lambda Pipeline getting started api"
    api_endpoint_config = {
      endpoint_types   = ["EDGE"]
      vpc_endpoint_ids = null
    }
    api_resources = {
      "/" = {
        get = {
          lambda            = "mock"
          status_code       = 200
          content_type      = "text/html"
          response_template = "<html>\n<body>\n<h1>Hello World!</h1>\n</body>\n</html>"
        }
      }
      "/books/{id}" = {
        get = {
          lambda         = "${data.terraform_remote_state.lambda_get_books.outputs.lambda.function_name}:$${stageVariables.alias}"
          lambda_aliases = ["default"]
          authorizer     = null
        }
      }
    }
  }
  stages = {
    "default" = {
      stage_variables = {
        alias = "default"
      }
      xray_tracing_enabled = false
    }
  }
}
