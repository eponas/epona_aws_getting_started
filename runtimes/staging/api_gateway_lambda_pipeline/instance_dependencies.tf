data "terraform_remote_state" "lambda_get_books" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "lambda_api_gateway/get_books/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}
