provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "cd_pipeline_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
  delivery_account_id = "777221001925"

  pipeline_name = "epona-frontend-cd-pipeline"

  artifact_store_bucket_name = "epona-frontend-artifacts"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_frontend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "arn:aws:iam::777221001925:role/Epona-Frontend-Cd-PipelineAccessRole"

  # ci_pipeline の bucket_name と同じ名前を指定する
  source_bucket_name = "epona-static-resource"
  source_object_key  = "source.zip"

  deployment_bucket_name = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.frontend_bucket_name
  deployment_object_path = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.frontend_bucket_origin_path

  deployment_require_approval = false

  cache_invalidation_config = {
    enable                     = true
    cloudfront_distribution_id = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.cloudfront_id
  }
}
