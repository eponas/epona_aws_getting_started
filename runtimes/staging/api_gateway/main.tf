provider "aws" {
  alias = "default_provider"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias  = "cloudfront_provider"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "api_gateway_rest_api" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/api_gateway?ref=v0.2.6"

  providers = {
    aws.default_provider    = aws.default_provider
    aws.cloudfront_provider = aws.cloudfront_provider
  }

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  api_gateway_settings = {
    api_name        = "epona-api-example"
    api_description = "Epona getting started api"
    api_endpoint_config = {
      endpoint_types   = ["EDGE"]
      vpc_endpoint_ids = null
    }
    api_resources = {
      "/" = {
        get = {
          lambda            = "mock"
          status_code       = 200
          content_type      = "text/html"
          response_template = "<html>\n<body>\n<h1>Hello World!</h1>\n</body>\n</html>"
        }
      }
      "/books" = {
        get = {
          lambda     = data.terraform_remote_state.lambda_list_books.outputs.lambda.function_name
          authorizer = null
        }
        post = {
          lambda     = data.terraform_remote_state.lambda_post_books.outputs.lambda.function_name
          authorizer = "example_authorizer"
        }
      }
      "/books/{id}" = {
        get = {
          lambda         = "${data.terraform_remote_state.lambda_get_books.outputs.lambda.function_name}:$${stageVariables.alias}"
          lambda_aliases = ["default"]
          authorizer     = null
        }
      }
    }
    api_security_schemes = {
      example_authorizer = {
        authorizer_parameter_name = "authorizer_token"
        authorizer_parameter_in   = "header"
        request_or_token_type     = "token"
        request_identity_source   = null
        authorizer_function_name  = data.terraform_remote_state.lambda_authorizer.outputs.lambda.function_name
        authorizer_credentials    = aws_iam_role.invocation_role.arn
        authorizer_ttl            = "300"
      }
    }
  }
  stages = {
    "default" = {
      stage_variables = {
        alias = "default"
      }
      xray_tracing_enabled = false
    }
  }
  custom_domains = {
    "api-example.epona-devops.com" = {
      custom_zone_name = "epona-devops.com"
      base_path_mapping = {
        "/staging" = {
          stage_name = "default"
        }
      }
    }
  }
}

resource "aws_iam_role" "invocation_role" {
  provider = aws.default_provider

  name = "epona_example_auth_invocation"
  path = "/"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "apigateway.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy" "invocation_policy" {
  provider = aws.default_provider

  name = "epona-example-auth-invocation-policy"
  role = aws_iam_role.invocation_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "lambda:InvokeFunction",
      "Effect": "Allow",
      "Resource": "${data.terraform_remote_state.lambda_authorizer.outputs.lambda.arn}"
    }
  ]
}
EOF
}
