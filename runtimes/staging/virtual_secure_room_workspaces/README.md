# virtual_secure_room_workspaces

本ページでは、`virtual_secure_room_workspaces`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`virtual_secure_room_workspaces`で使用しているpatternモジュールは、以下となります。

- [virtual_secure_room_workspaces pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_workspaces/)

## terraform applyを実行する前に

### ディレクトリサービスのユーザーログイン名を設定する

本モジュールを実行する前に、ディレクトリサービスにユーザーを作成する必要があります。
まだの場合は、[virtual_secure_room_directory_service patternのガイド](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_directory_service/#%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF%E3%83%88%E3%83%AA%E3%83%A6%E3%83%BC%E3%82%B6%E3%83%BC%E3%81%AE%E8%BF%BD%E5%8A%A0%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)を参考にユーザーを作成してください。

ユーザーの作成が完了している場合は、そのユーザーのログイン名を`workspaces_user_names`に設定してください。
仮にログイン名が`test.taro`の場合は、次のように設定します。

```terraform
  ...
  workspaces_user_names = ["test.taro"]
  ...
```

### 接続を許可する送信元IPアドレスを設定する

Workspacesは、設定されたIPアドレスからしか接続できないように構築されます。
したがって、あらかじめ接続元のIPアドレスを確認しておく必要があります。

自宅PCなどグローバルIPアドレスが不定な環境の場合は、[アクセス情報【使用中のIPアドレス確認】 | CMAN](https://www.cman.jp/network/support/go_access.cgi)などのサイトで現在のIPアドレスを確認できます。

IPアドレスはリポジトリにコミットしたくないことが考えられるため、ここでは`terraform.tfvars`という入力用のファイルを用意します。
（`terraform.tfvars`は`.gitignore`で除外対象になっているため、コミット対象からは外れます）

本ディレクトリ内の`terraform.tfvars_sample`をコピーして、`terraform.tfvars`を作成してください。

仮に接続元のIPアドレスが`11.22.33.44`だったとします。
その場合、`terraform.tfvars`の中を以下のように設定することで、接続を許可できるようになります。

```terraform
source_addresses = [
  {
    source_ip   = "11.22.33.44/32",
    description = "work pc"
  }
]
```
