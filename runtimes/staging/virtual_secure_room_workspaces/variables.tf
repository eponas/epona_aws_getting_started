variable "source_addresses" {
  description = "Workspacesにアクセス可能な送信元のCIDR"
  type = list(object({
    source_ip   = string
    description = string
  }))
}
