provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "workspaces" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/virtual_secure_room_workspaces?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  workspaces_directory_id = data.terraform_remote_state.staging_directory_service.outputs.directory_service.directory_id
  workspaces_subnet_ids   = data.terraform_remote_state.staging_operation_network.outputs.network.private_subnets
  # 先にDirectory Serviceでユーザーを作成してください。
  workspaces_user_names                                = ["kuwano.toru"]
  workspaces_kms_key                                   = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/common-encryption-key"].key_arn
  workspaces_compute_type_name                         = "VALUE"
  workspaces_user_volume_size_gib                      = 50
  workspaces_root_volume_size_gib                      = 80
  workspaces_running_mode                              = "AUTO_STOP"
  workspaces_running_mode_auto_stop_timeout_in_minutes = 120

  workspaces_access_source_addresses = var.source_addresses

}
