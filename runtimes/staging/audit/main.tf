provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "audit" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/audit?ref=v0.2.6"

  trail_name = "epona-staging-cloudtrail"
  trail_tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  bucket_name = "epona-staging-trail-bucket"

  kms_key_alias_name = "alias/epona-trail-encryption-key"
  kms_key_management_arns = [
    "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  ]
}
