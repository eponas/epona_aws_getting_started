provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }

  region = "us-east-1"
}

module "datadog_log_trigger_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_log_trigger?ref=v0.2.6"

  datadog_forwarder_lambda_arn = data.terraform_remote_state.staging_datadog_forwarder_frontend.outputs.datadog_forwarder_frontend.datadog_forwarder_lambda_function_arn

  cloudwatch_log_subscription_filters = []

  logging_s3_bucket_notifications = [
    {
      bucket = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.cloudfront_logging_bucket
    },
    {
      bucket = data.terraform_remote_state.staging_webacl_frontend.outputs.webacl.webacl_traffic_log_bucket
    },
  ]
}
