provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "step_functions" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/step_functions?ref=v0.2.6"

  sfn_name        = "epona-example-sfn"
  definition_json = file("sample-definition.json")
  log_group_name  = "epona-example-sfn-log"

  execution_resource_arns = [
    data.terraform_remote_state.lambda_step_functions_get_csv.outputs.lambda.arn
  ]
  s3_event_configs = [
    {
      bucket_name = data.terraform_remote_state.lambda_step_functions_get_csv.outputs.s3.bucket
      events      = ["PutObject"]
    }
  ]

  # Slack通知に関する設定を行うブロックです。Slack通知を使用する場合はこのブロックをコメントインし、webhook_uriに値を設定してください。
  #  slack_notification_configs = [
  #    {
  #      # Getting Startedとして共通で使用できるURLはないため未設定としています。各PJで用意しているIncoming WebhookのURLを設定してください。
  #      webhook_uri                      = ""
  #      input_transformer_input_paths    = null
  #      input_transformer_input_template = null
  #      event_bus                        = "default"
  #      event_pattern_status             = ["SUCCEEDED"]
  #    }
  #  ]

  # Microsoft Teams通知に関する設定を行うブロックです。Microsoft Teams通知を使用する場合はこのブロックをコメントインし、webhook_uriに値を設定してください。
  #  teams_notification_configs = [
  #    {
  #      # Getting Startedとして共通で使用できるURLはないため未設定としています。各PJで用意しているIncoming WebhookのURLを設定してください。
  #      webhook_uri                      = ""
  #      input_transformer_input_paths    = null
  #      input_transformer_input_template = null
  #      event_bus                        = "default"
  #      event_pattern_status             = ["SUCCEEDED"]
  #    }
  #  ]
}
