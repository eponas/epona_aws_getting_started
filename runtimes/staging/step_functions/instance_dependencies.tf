data "terraform_remote_state" "lambda_step_functions_get_csv" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "lambda_step_functions/get_csv/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}
