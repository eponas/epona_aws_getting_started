provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "roles" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/roles?ref=v0.2.6"

  account_id = "777221001925"

  admins       = ["alice"]
  approvers    = ["bob"]
  costmanagers = ["charlie"]
  developers   = ["dave"]
  operators    = ["ellen"]
  viewers      = ["frank"]

  system_name = "epona"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
