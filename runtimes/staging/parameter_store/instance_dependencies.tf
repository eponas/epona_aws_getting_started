data "terraform_remote_state" "staging_encryption_key" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "encryption_key/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}
