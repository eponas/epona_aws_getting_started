variable "nablarch_db_url" {
  type = string
}

variable "nablarch_db_user" {
  type = string
}

variable "nablarch_db_password" {
  type = string
}

variable "nablarch_db_schema" {
  type = string
}

variable "websocket_url" {
  type = string
}

variable "mail_smtp_host" {
  type = string
}

variable "mail_smtp_port" {
  type = number
}

variable "mail_smtp_user" {
  type = string
}

variable "mail_smtp_password" {
  type = string
}

variable "mail_from_address" {
  type = string
}

variable "mail_returnpath" {
  type = string
}

variable "application_external_url" {
  type = string
}

variable "cors_origins" {
  type = string
}

variable "nablarch_sessionstorehandler_cookiesecure" {
  type = bool
}

variable "nablarch_lettuce_simple_url" {
  type = string
}

variable "datadog_api_key" {
  type = string
}
