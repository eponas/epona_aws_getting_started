# parameter_store

本ページでは、`parameter_store`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`parameter_store`で適用しているpatternモジュールは以下となります。

- [parameter_store pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/parameter_store/)

## 入力変数ファイルを作成する

`parameter_store pattern`に入力する値には、データベースのパスワードなどの秘匿情報が含まれます。  
[秘匿情報の管理](https://eponas.gitlab.io/epona/guide/patterns/aws/parameter_store/#%E7%A7%98%E5%8C%BF%E6%83%85%E5%A0%B1%E3%81%AE%E7%AE%A1%E7%90%86)に記載されているように、Eponaにおいてはこれらの秘匿情報を入力変数ファイルに記載し、管理を別にすることを推奨しています。

したがって、本ディレクトリ内に定義されたモジュールを実行するためには、入力となるファイルを用意する必要があります。
また、ファイル内に設定する内容は他のモジュール（※インスタンスと呼びます）の入出力が必要なものもあります。

:information_source: インスタンスという言葉については、[インスタンス化](https://eponas.gitlab.io/epona/guide/patterns/#%E3%82%A4%E3%83%B3%E3%82%B9%E3%82%BF%E3%83%B3%E3%82%B9%E5%8C%96)を参照してください。

本ディレクトリに`terraform.tfvars`という名前でファイルを作成し、以下の項目を設定します。

:information_source: 設定値内のプレースホルダー`{}`については、`「設定値」で使用しているプレースホルダは、以下のように読み替えてください。`を参照してください。

| 変数名 | 設定値 | 説明 |
|---|---|---|
| `nablarch_db_url` | `jdbc:postgresql://{database.outputs.instance_endpoint}/{database.inputs.name}` | アプリがJDBC接続で使用するURL |
| `nablarch_db_user` | `{database.inputs.username}` | DBの接続ユーザー名 |
| `nablarch_db_password` | `{database.inputs.password}` | DBの接続パスワード |
| `nablarch_db_schema` | `public` | DBの接続スキーマ |
| `websocket_url` | `wss://{public_traffic_container_service_notifier.inputs.dns.record_name}/notification` | フロントエンドがnotifierに接続するときのURL |
| `mail_smtp_host` | `email-smtp.ap-northeast-1.amazonaws.com` | SMTPホスト |
| `mail_smtp_port` | `587` | SMTPポート |
| `mail_smtp_user` | `{ses_smtp_user.outputs.access_key_id}` | SMTPユーザ |
| `mail_smtp_password` | `{ses_smtp_user.outputs.smtp_password_v4}` | SMTPパスワード |
| `application_external_url` | `https://{cacheable_frontend.inputs.record_name}` | アプリケーションのベースURL |
| `nablarch_lettuce_simple_url` | `rediss://{redis.inputs.auth_token}@{redis.outputs.elasticache_redis_primary_endpoint_address}:6379` | Redisへの接続で使用するURL |
| `mail_from_address` | 任意のメールアドレス | メールのFrom |
| `mail_returnpath` | 任意のメールアドレス | メールのReply-To |
| `cors_origins` | `https://{cacheable_frontend.inputs.record_name}` | フロントエンドのオリジン(CORS用) |
| `nablarch_sessionstorehandler_cookiesecure` | `true` | CookieのSecure属性 |
| `datadog_api_key` | Datadogで発行したAPIキー | Datadog Agent連携用のAPIキー |

:information_source:`mail_smtp_port`以外の設定値は全てダブルクォーテーションで括る必要があります。

```terraform
# terraform.tfvarsの例（一部）
...
mail_smtp_host="email-smtp.ap-northeast-1.amazonaws.com"
mail_smtp_port=587
...
nablarch_sessionstorehandler_cookiesecure="true"
...
```

:information_source:「設定値」で使用しているプレースホルダは、以下のように読み替えてください。

- `{インスタンス名.inputs.属性名}`
  - 当該インスタンスの入力で設定した属性値に置き換える
- `{インスタンス名.outputs.属性名}`
  - 当該インスタンスの出力結果の属性値に置き換える

「インスタンス名」は、`pattern`モジュールを適用しているディレクトリの名前と一致します。
必要な設定値は、それぞれの`pattern`モジュールを適用したディレクトリ内のファイルや結果を確認してください。

変数`nablarch_db_url`を例にしましょう。
この変数には、`database`インスタンスへの入出力を使って次の値を設定します。

- `jdbc:postgresql://{database.outputs.instance_endpoint}/{database.inputs.name}`

このとき、`database`インスタンスの入力と出力が以下のような内容だったとします。

```terraform
# 入力(runtimes/staging/database/main.tf)
module "database" {
  ...
  name = "test_postgres"
  ...
```

```shell
# 出力
$ cd runtimes/staging/database

$ terraform output
database = {
  "instance_address" = "..."
  "instance_endpoint" = "foo.bar.com:5432"
...
```

プレースホルダには、入力の`name`と出力の`instance_endpoint`の値を使用します。
したがって、`nablarch_db_url`に設定する値は`jdbc:postgresql://foo.bar.com:5432/test_postgres`となります。

:information_source:`datadog_api_key`は、Datadog Agentによるメトリクスを連携する場合に設定してください。詳しくは[こちら](https://eponas.gitlab.io/epona/guide/how_to/aws/send_ecs_fargate_metrics_to_datadog/)を参照してください。

入力変数ファイルの作成が完了したら、`terraform apply`を実行してください。
