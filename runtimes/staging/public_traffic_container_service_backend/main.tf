provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "public_traffic_container_service_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/public_traffic_container_service?ref=v0.2.6"

  name = "epona-chat-example-backend"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  vpc_id                             = data.terraform_remote_state.staging_network.outputs.network.vpc_id
  public_subnets                     = data.terraform_remote_state.staging_network.outputs.network.public_subnets
  public_traffic_protocol            = "HTTPS"
  public_traffic_port                = 443
  create_public_traffic_certificate  = true
  public_traffic_inbound_cidr_blocks = ["0.0.0.0/0"]

  public_traffic_access_logs_force_destroy = false

  dns = {
    zone_name   = "epona-devops.com"
    record_name = "chat-example-backend.staging.epona-devops.com"
  }

  container_subnets  = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  container_protocol = "HTTP"
  container_port     = 80

  container_health_check_path           = "/api/health"
  container_cluster_name                = "epona-chat-example-backend"
  container_traffic_inbound_cidr_blocks = ["10.251.0.0/16"]
  container_service_desired_count       = 1
  container_service_platform_version    = "1.4.0"
  container_task_cpu                    = 512
  container_task_memory                 = "1024"

  default_ecs_task_iam_role_name             = "EponaBackendContainerServiceTaskRole"
  default_ecs_task_iam_policy_name           = "EponaBackendContainerServiceTaskRolePolicy"
  default_ecs_task_execution_iam_policy_name = "EponaBackendContainerServiceTaskExecution"
  default_ecs_task_execution_iam_role_name   = "EponaBackendContainerServiceTaskExecutionRole"

  container_log_group_names = [
    "/epona/fargate/epona-chat-example-backend/chat-example-backend",
    "/epona/fargate/epona-chat-example-backend/fluent-bit",
    "/epona/fargate/epona-chat-example-backend/datadog-agent",
    "/epona/fargate/epona-chat-example-backend/reverse-proxy"
  ]

  container_definitions = <<-JSON
  [
    {
      "name": "epona-chat-example-backend-task",
      "image": "hashicorp/http-echo:0.2.3",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": 80
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "/epona/fargate/epona-chat-example-backend/chat-example-backend",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "http-echo"
        }
      },
      "command": [
          "-listen",
          ":80",
          "-text",
          "echo"
      ]
    }
  ]
  JSON
}
