# ses_smtp_user

本ページでは、本リポジトリ独自で作成している`ses_smtp_user pattern`とその利用方法について説明します。

## 概要

本ディレクトリは、独自に作成した`ses_smtp_user pattern`を使用しています。  
このpatternの実装は、[modules/patterns/ses_smtp_user](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/runtimes/staging/ses_smtp_user)にあります。

example-chatは、メールの送信に[Amazon Simple Email Service](https://aws.amazon.com/jp/ses/)(以下SES)を使用します。
`ses_smtp_user pattern`は、SESでメールを送信するためのIAMユーザーおよび認証情報を作成します。

## 構築されるリソース

`ses_smtp_user pattern`を実行すると、以下のリソースが構築されます。

| リソース名 | 説明 |
|---|---|
| [IAMユーザー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_users.html) | SESのSMTP認証を行うためのIAMユーザーを作成します |
| [IAMポリシー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/access_policies.html) | IAMユーザーに、SESを使用してメールを送信するためのIAMポリシーを付与します |
| [アクセスキー](https://docs.aws.amazon.com/ja_jp/IAM/latest/UserGuide/id_credentials_access-keys.html) | SESのSMTP認証情報として使用する、IAMユーザーのアクセスキーを作成します |

:information_source: SESのSMTP認証については、下記ページを参考にしてください。

- [Amazon SES SMTP 認証情報の取得](https://docs.aws.amazon.com/ja_jp/ses/latest/DeveloperGuide/smtp-credentials.html)
- [種類 Amazon SES 資格情報](https://docs.aws.amazon.com/ja_jp/ses/latest/DeveloperGuide/send-email-concepts-credentials.html)

## 独自のpatternの実装・使用例

example-chatにはSMTPを使ったメール送信の機能があり、このリポジトリではメール送信のサービスとしてSESを使用します。

SESのSMTPインタフェースを使用するためには、必要な権限を持ったIAMユーザーおよびアクセスキーを用意しなくてはいけません。  
よって、SES用のIAMユーザーおよびアクセスキーを作成するモジュールが必要になりますが、Eponaではこのようなpatternモジュールを提供していません。

Eponaは、クラウドリソースに対するあらゆるモジュールを提供しているわけではありません。  
このような場合、Epona利用者がEponaとサービスで必要なリソースの間を埋める必要があります。

`ses_smtp_user pattern`は、Eponaと実際のサービスの間を埋めるために、Epona利用者が独自に作成するpatternモジュールの具体例です。

:information_source: patternの実装については、[modules/patterns/ses_smtp_user](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/modules/patterns/ses_smtp_user)を参照してください。

また、独自patternを実装する方法については[Eponaのpatternモジュールで不足する場合](https://eponas.gitlab.io/epona/guide/patterns/#epona%E3%81%AEpattern%E3%83%A2%E3%82%B8%E3%83%A5%E3%83%BC%E3%83%AB%E3%81%A7%E4%B8%8D%E8%B6%B3%E3%81%99%E3%82%8B%E5%A0%B4%E5%90%88)を参照してください。

本ディレクトリおよび[modules/patterns/ses_smtp_user](https://gitlab.com/eponas/epona_aws_getting_started/-/tree/master/modules/patterns/ses_smtp_user)は、Epona利用者が作成する独自のpatternおよびその実装例として参考にしてください。
