provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "ses_smtp_user" {
  source = "../../../modules/patterns/ses_smtp_user"

  username    = "ChatExampleSesSmtpUser"
  policy_name = "ChatExampleSesSmtpPolicy"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
