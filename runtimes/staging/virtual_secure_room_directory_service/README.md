# virtual_secure_room_directory_service

本ページでは、`virtual_secure_room_directory_service`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`virtual_secure_room_directory_service`で使用しているpatternモジュールは、以下となります。

- [virtual_secure_room_directory_service pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_directory_service/)

## terraform applyを実行する前に

### 接続を許可する送信元IPアドレスを設定する

AD Manager用の[Amazon EC2](https://aws.amazon.com/jp/ec2/)インスタンスは、設定されたIPアドレスからしか接続できないように構築されます。
したがって、あらかじめ接続元のIPアドレスを確認しておく必要があります。

自宅PCなどグローバルIPアドレスが不定な環境の場合は、[アクセス情報【使用中のIPアドレス確認】 | CMAN](https://www.cman.jp/network/support/go_access.cgi)などのサイトで現在のIPアドレスを確認できます。

IPアドレスはリポジトリにコミットしたくないことが考えられるため、ここでは`terraform.tfvars`という入力用のファイルを用意します。
（`terraform.tfvars`は`.gitignore`で除外対象になっているため、コミット対象からは外れます）

本ディレクトリ内の`terraform.tfvars_sample`をコピーして、`terraform.tfvars`を作成してください。

仮に接続元のIPアドレスが`11.22.33.44`だったとします。
その場合、`terraform.tfvars`の中を以下のように設定することで、接続を許可できるようになります。

```terraform
ad_manager_allow_cidr_blocks = [
  {
    port        = 3389,
    protocol    = "TCP",
    cidr_blocks = ["11.22.33.44/32"],
    description = "AD Manager inbound SG Rule"
  }
]
```

## terraform applyを実行した後に

### ディレクトリサービスのユーザーを作成する

本モジュールを実行した後に、ディレクトリサービスのユーザーを作成する必要があります。
具体的な手順は、[virtual_secure_room_directory_service patternのガイド](https://eponas.gitlab.io/epona/guide/patterns/aws/virtual_secure_room_directory_service/#%E3%83%87%E3%82%A3%E3%83%AC%E3%82%AF%E3%83%88%E3%83%AA%E3%83%A6%E3%83%BC%E3%82%B6%E3%83%BC%E3%81%AE%E8%BF%BD%E5%8A%A0%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)を参照してください。

ここで作成したユーザーの「ユーザーログオン名」は、次の`virtual_secure_room_workspaces`の入力パラメータで必要になるのでメモしておいてください。
