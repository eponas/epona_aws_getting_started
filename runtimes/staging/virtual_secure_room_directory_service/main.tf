provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "directory_service" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/virtual_secure_room_directory_service?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  directory_service_domain_name     = "staging.epona-devops.com"
  directory_service_domain_password = "P@ssw0rd1234"
  directory_service_short_name      = "EPONA"
  directory_service_ou              = "OU=EPONA,DC=staging,DC=epona-devops,DC=com"
  directory_service_vpc_id          = data.terraform_remote_state.staging_operation_network.outputs.network.vpc_id
  directory_service_subnet_ids      = data.terraform_remote_state.staging_operation_network.outputs.network.private_subnets

  ad_manager_name                   = "ad_manager"
  ad_manager_administrator_password = "P@ssw0rd1234"
  ad_manager_subnet_id              = data.terraform_remote_state.staging_operation_network.outputs.network.public_subnets[0]

  ad_manager_role_name             = "EponaADManagerRole"
  ad_manager_instance_profile_name = "EponaAdManagerInstanceProfile"

  ad_manager_sg_name      = "epona_ad_manager_sg"
  ad_manager_sg_ingresses = var.ad_manager_allow_cidr_blocks
}
