variable "ad_manager_allow_cidr_blocks" {
  description = "Directory Serviceにアクセス可能な送信元のCIDR"
  type = list(object({
    port        = number
    protocol    = string
    cidr_blocks = list(string)
    description = string
  }))
}
