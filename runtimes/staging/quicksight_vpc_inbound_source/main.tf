provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "quicksight_vpc_inbound_source" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/quicksight_vpc_inbound_source?ref=v0.2.6"

  security_group_name = "Epona-QuickSight-SG"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  integration_security_groups = [{
    description       = "Enable access postgresql from QuickSight"
    security_group_id = data.terraform_remote_state.staging_database.outputs.database.instance_security_group_id
  }]

  vpc_id = data.terraform_remote_state.staging_network.outputs.network.vpc_id
}
