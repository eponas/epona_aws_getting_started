provider "aws" {
  region = "ap-northeast-1"

  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "vpc_peering" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/vpc_peering?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  name             = "development-operation-peering"
  requester_vpc_id = data.terraform_remote_state.staging_development_network.outputs.network.vpc_id
  accepter_vpc_id  = data.terraform_remote_state.staging_operation_network.outputs.network.vpc_id

}
