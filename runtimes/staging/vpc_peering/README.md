# vpc_peering

本ページでは、Staging環境の`vpc_peering`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`vpc_peering`で使用しているpatternモジュールは、以下となります。

- [vpc_peering pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/vpc_peering/)

## terraform applyを実行した後に

本モジュールで構築したVPCピアリングのIDは、以下のモジュールに反映する必要があります。

- [runtimes/staging/network](../network)
- [runtimes/staging/operation_network](../operation_network)

それぞれのモジュールは、初回実行時に本モジュールと依存している部分をコメントアウトして実行しています。
本モジュールを実行した後は、各モジュールでコメントアウトしていた部分をコメントインしなおし、再実行してください。
