provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "datadog_forwarder" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_forwarder?ref=v0.2.6"

  providers = {
    aws.ssm_provider       = aws
    aws.datadog_deployment = aws
  }

  dd_api_key_ssm_parameter_name = data.terraform_remote_state.staging_parameter_store.outputs.parameter_store.parameters["/Epona/Infra/Config/SaaS/Datadog/datadog_api_key"].name

  datadog_forwarder_tags = "environment:runtime,runtime_environment:staging,collected_by:epona"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
