provider "aws" {
  # CloudFrontとWAFを紐付ける場合、us-east-1のリージョンにWAFを作成する必要があります
  alias  = "global"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

# terraformのAWS WAFv2を利用して、本モジュールで適用するルールグループを作成
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_rule_group
# 下記は、SQLインジェクションが含まれるリクエストをブロックする例
resource "aws_wafv2_rule_group" "this" {
  provider = aws.global

  name        = "chat-example-frontend-waf-managed-rule_group"
  description = "Chat example frontend waf rule group"
  scope       = "CLOUDFRONT"
  capacity    = 60

  rule {
    name     = "ChatExampleFrontendBlockSqliRule"
    priority = 1

    # ルール違反のリクエストがある場合はblockする
    action {
      block {}
    }

    statement {
      sqli_match_statement {
        field_to_match {
          all_query_arguments {}
        }

        text_transformation {
          priority = 1
          type     = "URL_DECODE"
        }
        text_transformation {
          priority = 2
          type     = "HTML_ENTITY_DECODE"
        }
        text_transformation {
          priority = 3
          type     = "COMPRESS_WHITE_SPACE"
        }
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "chat-example-fronend-rule-metrics"
      sampled_requests_enabled   = true
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = "chat-example-fronend-rule_group-metrics"
    sampled_requests_enabled   = true
  }
}

module "webacl_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/webacl?ref=v0.2.6"

  providers = {
    aws = aws.global
  }

  # ルール外のアクセスは許可
  default_action = "allow"
  name           = "chat-example-frontend-webacl"
  scope          = "CLOUDFRONT"
  rule_groups = [
    {
      arn                        = aws_wafv2_rule_group.this.arn
      override_action            = "none"
      cloudwatch_metrics_enabled = true
      sampled_requests_enabled   = true
    }
  ]

  web_acl_cloudwatch_metrics_enabled = true
  web_acl_sampled_requests_enabled   = true

  create_logging_bucket      = true
  s3_logging_bucket_name     = "chat-example-frontend-acl-firehose-bucket"
  logging_prefix             = "frontend_waf_log/"
  logging_compression_format = "GZIP"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}
