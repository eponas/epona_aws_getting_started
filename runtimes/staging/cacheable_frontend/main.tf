# OriginとなるS3バケットを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias = "origin_provider"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias  = "cache_provider"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "cacheable_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cacheable_frontend?ref=v0.2.6"

  # Provider定義をmoduleに伝播
  providers = {
    aws.origin_provider = aws.origin_provider
    aws.cache_provider  = aws.cache_provider
  }

  s3_frontend_bucket_name = "chat-example-frontend-static"
  zone_name               = "epona-devops.com"
  record_name             = "chat-example.staging.epona-devops.com"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  cloudfront_default_root_object = "index.html"
  cloudfront_origin = {
    origin_path = "/public"
  }

  cloudfront_default_cache_behavior = {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = null
    default_ttl            = null
    max_ttl                = null
    compress               = false
  }

  cloudfront_viewer_certificate = {
    minimum_protocol_version = "TLSv1.2_2019"
  }

  cloudfront_custom_error_responses = [
    {
      error_code            = 404
      error_caching_min_ttl = 300
      response_code         = 200
      response_page_path    = "/"
    }
  ]

  cloudfront_logging_config = {
    bucket_name           = "chat-example-frontend-logging"
    create_logging_bucket = true
    prefix                = "chat-example-frontend"
    include_cookies       = false
  }

  # webacl_frontendで作成したAWS WAFをCloudFrontと紐付け
  cloudfront_web_acl_id = try(data.terraform_remote_state.staging_webacl_frontend.outputs.webacl.webacl_arn, null)

  # レスポンスヘッダーに情報を付与するラムダ関数名
  viewer_response_lambda_function_name = "chat-example-frontend-add-header-function"
}
