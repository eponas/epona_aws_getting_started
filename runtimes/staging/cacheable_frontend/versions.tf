terraform {
  required_version = "0.14.10"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.37.0"
    }
    archive = {
      source  = "hashicorp/archive"
      version = "2.2.0"
    }
  }

  backend "s3" {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "cacheable_frontend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}
