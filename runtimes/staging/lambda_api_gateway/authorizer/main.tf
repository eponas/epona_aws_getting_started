provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "lambda" {
  #  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  function_name = "epona-example-lambda-authorizer"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  source_dir    = "./node"

  environment = {
    env : "staging"
  }
}
