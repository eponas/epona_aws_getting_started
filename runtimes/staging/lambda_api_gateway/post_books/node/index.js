// API Path /books
// API Method POST

console.log('Loading function');

const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  const requestJSON = JSON.parse(event.body);
  const body = await dynamo
  .put({
    TableName: 'epona-api-example-books',
    Item: {
      id: requestJSON.id,
      title: requestJSON.title,
      price: requestJSON.price,
    },
  })
  .promise();
  console.log(body)

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  }
};
