provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "lambda" {
  #  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  function_name = "epona-example-lambda-list-books"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  source_dir    = "./node"

  environment = {
    env : "staging"
  }
}

resource "aws_iam_policy" "dynamodb_scan_policy" {
  name = "epona-example-dynamodb-scan-policy"
  path = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "dynamodb:Scan"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}

resource "aws_iam_role_policy_attachment" "dynamodb_role" {
  role       = module.lambda.role_name
  policy_arn = aws_iam_policy.dynamodb_scan_policy.arn
}
