// API Path /books
// API Method GET

console.log('Loading function');

const AWS = require('aws-sdk');
const dynamo = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event, context, callback) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  // 本ファイルはあくまでサンプルであるため簡易に実装しています。scan の使用は推奨されていません。
  // DynamoDBからデータを取得する際は、本来はページングやパフォーマンスを考慮して実装する必要があります
  // 参考: https://docs.aws.amazon.com/ja_jp/amazondynamodb/latest/developerguide/bp-query-scan.html
  const body = await dynamo.scan({TableName: 'epona-api-example-books'}).promise();
  console.log(body)

  return {
    statusCode: 200,
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      books: body.Items
    }),
  }
}
