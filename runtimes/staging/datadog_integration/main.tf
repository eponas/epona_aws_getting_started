provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

provider "datadog" {
}

module "datadog_integration" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_integration?ref=v0.2.6"

  integrate_aws_account_id = "232105380006"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  filter_tags = ["datadog:monitored"]
}
