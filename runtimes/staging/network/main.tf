provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.6"

  name = "epona-staging-network"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  cidr_block = "10.251.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c"]

  public_subnets             = ["10.251.1.0/24", "10.251.2.0/24"]
  private_subnets            = ["10.251.3.0/24", "10.251.4.0/24"]
  nat_gateway_deploy_subnets = ["10.251.1.0/24", "10.251.2.0/24"]

  # peering関連のパラメータはvpc_peering pattern実行後に設定する。
  # vpc_peering pattern実行前は以下パラメータをコメントアウトにする。
  peering_id   = data.terraform_remote_state.vpc_peering.outputs.vpc_peering.peering_id
  peering_cidr = "10.252.0.0/16"
}
