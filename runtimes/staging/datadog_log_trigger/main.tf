provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "datadog_log_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/datadog_log_trigger?ref=v0.2.6"

  datadog_forwarder_lambda_arn = data.terraform_remote_state.staging_datadog_forwarder.outputs.datadog_forwarder.datadog_forwarder_lambda_function_arn

  cloudwatch_log_subscription_filters = concat(
    ## VPCフローログは、いったん収集対象から外す
    # [
    #   {
    #     log_group_name = data.terraform_remote_state.staging_network.outputs.network.flow_log_cloudwatch_log_group_name
    #   }
    # ],
    [for name in data.terraform_remote_state.staging_database.outputs.database.instance_log_group_names : {
      log_group_name = name
    }],
    [
      {
        log_group_name = data.terraform_remote_state.staging_database.outputs.database.monitoring_log_group_name
      }
    ],
    [for name in data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service.cloudwatch_container_log_names : {
      log_group_name = name
    }],
    [for name in data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service.cloudwatch_container_log_names : {
      log_group_name = name
    }],
    [
      {
        log_group_name = data.terraform_remote_state.staging_cd_pipeline_frontend.outputs.cd_pipeline_frontend.codebuild.cloudwatch_logs.log_group_name
      }
    ],
    # 以下は、初回はコメントアウトして適用し、 rule_violation 適用後にコメントインして再適用してください
    [
      {
        log_group_name = data.terraform_remote_state.staging_rule_violation.outputs.rule_violation.lambda_function_rule_violation_log_group_name
      }
    ],
    # 以下は、初回はコメントアウトして適用し、 threat_detection 適用後にコメントインして再適用してください
    [
      {
        log_group_name = data.terraform_remote_state.staging_threat_detection.outputs.threat_detection.lambda_function_thread_detection_log_group_name
      }
    ],
  )

  logging_s3_bucket_notifications = [
    {
      bucket = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service.load_balancer_access_logs_bucket
    },
    {
      bucket = data.terraform_remote_state.staging_public_traffic_container_service_notifier.outputs.public_traffic_container_service.load_balancer_access_logs_bucket
    },
    {
      bucket = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.s3_access_log_bucket
    },
  ]
}
