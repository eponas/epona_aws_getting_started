# datadog_log_trigger

本ページでは、`datadog_log_trigger`を実行するにあたっての注意事項やポイントを記載します。

## 適用しているpattern

`datadog_log_trigger`で使用しているpatternモジュールは、以下となります。

- [datadog_log_trigger pattern](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_log_trigger/)

## terraform applyを実行する前に

本インスタンスには、この後の手順で実施する以下のインスタンスに依存している箇所があります。

- `runtimes/staging/rule_violation`
- `runtimes/staging/threat_detection`

したがって、初回適用時は依存部分をコメントアウトして実行する必要があります。

依存部分を含むファイルは、`instance_dependencies.tf`と`main.tf`です。  
それぞれ`以下は、初回はコメントアウトして適用し、～`と書かれた部分をコメントアウトしてから`terraform apply`を実行してください。

## 再適用のタイミング

再適用は、[Runtime環境の証跡やインフラ変更を記録、第三者による不正アクセスを通知する](../../../guide/2_apply_patterns.md#runtime環境の証跡やインフラ変更を記録第三者による不正アクセスを通知する)の後に実施します。

前述の手順でコメントアウトした部分をコメントインし、`terraform apply`を実行してください。

## ログフィルターについて

Datadogには[ログフィルター](https://eponas.gitlab.io/epona/guide/patterns/aws/datadog_log_trigger#ログフィルターについて)という機能があります。

Datadogの課金額が利用者の想定以上に増えることを避けるため、本インスタンスではVPCフローログに関する部分はコメントアウトしています。

ログフィルター設定後、必要に応じてVPCフローログに関する部分をコメントインし、`terraform apply`を実行してください。
