data "terraform_remote_state" "staging_network" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "network/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}

data "terraform_remote_state" "staging_encryption_key" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "encryption_key/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}

# セキュリティグループのインバウンドとなる以下stateは、関連pattern実行後に設定する。
# *1) virtual_secure_room_workspace pattern実行前は、以下のdataブロックをコメントアウトする。
data "terraform_remote_state" "staging_virtual_secure_room_workspaces" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "virtual_secure_room_workspaces/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}

# セキュリティグループのインバウンドとなる以下stateは、関連pattern実行後に設定する。
# *2) quicksight_vpc_inbound_source pattern実行前は、以下のdataブロックをコメントアウトする。
data "terraform_remote_state" "staging_quicksight_vpc_inbound_source" {
  backend = "s3"

  config = {
    bucket         = "epona-staging-terraform-tfstate"
    key            = "quicksight_vpc_inbound_source/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "epona_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::777221001925:role/StagingTerraformBackendAccessRole"
  }
}
