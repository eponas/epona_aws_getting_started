provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "database" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/database?ref=v0.2.6"

  name = "epona_postgres"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  identifier            = "epona-rds-instance"
  engine                = "postgres"
  engine_version        = "12.3"
  port                  = 5432
  instance_class        = "db.t3.micro"
  allocated_storage     = 5
  max_allocated_storage = 10

  deletion_protection = false
  skip_final_snapshot = true

  auto_minor_version_upgrade = true
  maintenance_window         = "Tue:18:00-Tue:18:30"
  backup_retention_period    = "0"
  backup_window              = "13:00-14:00" #  (JST)04:00-05:00

  username = "epona"
  password = "password"

  kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/common-encryption-key"].key_arn

  vpc_id            = data.terraform_remote_state.staging_network.outputs.network.vpc_id
  availability_zone = "ap-northeast-1a"

  db_subnets           = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  db_subnet_group_name = "epona-rds-db-subnet-group"

  parameter_group_name   = "epona-rds-parameter-group"
  parameter_group_family = "postgres12"

  option_group_name                 = "epona-rds-option-group"
  option_group_engine_name          = "postgres"
  option_group_major_engine_version = "12"

  performance_insights_kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/common-encryption-key"].key_arn

  # example parameters
  parameters = [
    { name = "max_connections", value = 200, apply_method = "pending-reboot" },
    { name = "shared_buffers", value = 40960, apply_method = "pending-reboot" }
  ]

  option_group_options = []

  # セキュリティグループのインバウンド設定は、関連pattern実行後に設定する。
  # 関連pattern実行前は、以下パラメータをコメントアウトする。
  source_security_group_ids = [
    data.terraform_remote_state.staging_virtual_secure_room_workspaces.outputs.workspaces.security_group_id,                  # *1) virtual_secure_room_workspace pattern適用前はコメントアウト
    data.terraform_remote_state.staging_quicksight_vpc_inbound_source.outputs.quicksight_vpc_inbound_source.security_group_id # *2) quicksight_vpc_inbound_source pattern適用前はコメントアウト
  ]
}
