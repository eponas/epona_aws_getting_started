provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::232105380006:role/TerraformExecutionRole"
  }
}

module "lambda" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/lambda?ref=v0.2.6"

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }

  function_name = "epona-sfn-lambda-getcsv"
  handler       = "index.handler"
  runtime       = "nodejs14.x"
  source_dir    = "./node"

  source_dir_exclude_files = [
    "package.json",
    "package-lock.json"
  ]
}

# Lambdaのファイル取得先
resource "aws_s3_bucket" "input_bucket" {
  bucket = "epona-sfn-input-csv"
}

# Lambdaのファイル出力先
resource "aws_s3_bucket" "output_bucket" {
  bucket = "epona-sfn-output-json"
}

resource "aws_iam_policy" "s3_access_policy" {
  name = "epona-example-lambda-s3-access-policy"
  path = "/"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:PutObject",
        "s3:GetObject"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF

  tags = {
    Owner              = "epona"
    Environment        = "runtime"
    RuntimeEnvironment = "staging"
    ManagedBy          = "epona"
  }
}

resource "aws_iam_role_policy_attachment" "s3_role" {
  role       = module.lambda.role_name
  policy_arn = aws_iam_policy.s3_access_policy.arn
}
