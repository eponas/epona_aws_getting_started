output "lambda" {
  value = module.lambda
}

output "s3" {
  value = aws_s3_bucket.input_bucket
}
