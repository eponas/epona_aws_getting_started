console.log('Loading function');

const AWS = require('aws-sdk');
const s3 = new AWS.S3({
  apiVersion: '2006-03-01',
  region: 'ap-northeast-1'
});
const stream = require('stream')
const csv = require('csvtojson')
const inputBucket = 'epona-sfn-input-csv';
const inputKey = 'test_data.csv';
const outputBucket = 'epona-sfn-output-json'
const outputKey = 'output_data.jsonl'

exports.handler = async (event) => {
  console.log('Received event:', JSON.stringify(event, null, 2));

  const uploadStream = () => {
    const pass = new stream.PassThrough();
    return {
      writeStream: pass,
      promise: s3.upload({
          Bucket: outputBucket,
          Key: outputKey,
          Body: pass,
        })
        .promise(),
    }
  }
  const { writeStream, promise } = uploadStream();
  const readStream = await s3.getObject({
    Bucket: inputBucket,
    Key: inputKey,
  })
  .createReadStream();

  readStream
    .pipe(csv())
    .pipe(writeStream);

  await promise;

  return {
    response_code: 0,
  };
};
